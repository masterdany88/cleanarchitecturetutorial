# Technology stack

 - api - JAX-RS Api interfaces and related Pojo dto classes
 - db - Docker compose project to run 1 docker container with 1 Postgresql server with multiply data bases (serverdb, server2), and credentials: user:user, password:password
 - gradle - Gradle build tool wrapper. You dont need to install Your own
 - server - Backend implementation of JAX-RS api
 - server / integration Contains Test containers IT. They run spring boot app locally an postgresqlpostgresql in container
 - ui - Frontend Web application using: GWT, NaluKit,
 
# Useful links:

[localhost:9001](http://localhost:9001/)

[localhost:9001/actuator](http://localhost:9001/actuator)

[localhost:9001/swagger-ui.html](http://localhost:9001/swagger-ui.html)

[localhost:9001/api/swagger.json](http://localhost:9001/api/swagger.json)

[localhost:9001/v2/api-docs](http://localhost:9001/v2/api-docs)

[localhost:9001/api/ping](http://localhost:9001/api/ping)

[localhost:9001/api/users/6](http://localhost:9001/api/users/6)

[localhost:9001/api/users/6/roles/2](http://localhost:9001/api/users/6/roles/2)

# you need db 
 postgres db on localhost:5432 is configured as default
 
 you can run it in container using `docker-compose`, execute:
 
 `cd db`
 
 `docker-compose up`
 
 it will run one db container, and can create many dbs on that server
 
 or You can just run:
 
`$ docker run --name serverDb -p 5432:5432 -e POSTGRES_DB=serverdb -e POSTGRES_PASSWORD=password -e POSTGRES_USER=user -d postgres`


# first compile gwt app/project (ui)

`./gradlew clean compileGwt`

# to run from sources:

`./gradlew clean bootRun`

Embedded tomcat server will run on `port 9000`

# to debug from sources:
 
 `./gradlew clean bootRun`
 
 Embedded tomcat server will run on `port 9000`

To debug backend server connect remote debugger to `port 9001`

To debug frontend part run -- WORK IN PROGRESS

`./gradlew gwtSuperDev`

# to build and run:

`./gradlew clean bootJar`

`cd server/build/libs`

`java -jar server-0.0.1-SNAPSHOT.jar`

