package pl.korbeldaniel.demo.api.resources;

import pl.korbeldaniel.demo.domain.model.UserDto;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Collection;

@Path("users")
@Produces(MediaType.APPLICATION_JSON)
public interface UsersResource {

    @GET
    Collection<UserDto> getUsers();

    @GET @Path("{id}")
    UserDto getUser(@PathParam("id") Long id);

    @POST
    UserDto createNewUser(UserDto userDto);

    @PUT
    @Path("{id}")
    void update(@PathParam("id") Long id, UserDto userDto);

    @GET @Path("test")
    UserDto getTestUser();

    @Path("{userId}/roles") UserRolesResource getUserRolesResource(@PathParam("userId") Long userId);
}
