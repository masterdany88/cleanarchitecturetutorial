package pl.korbeldaniel.demo.api.resources;

import pl.korbeldaniel.demo.domain.model.payment.PaymentMethodsResponse;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("buy")
public interface BuyResource {
    @GET
    PaymentMethodsResponse getPaymentsMethods();


}
