package pl.korbeldaniel.demo.api.resources;

import javax.ws.rs.POST;
import javax.ws.rs.Path;

@Path("emails") public interface EmailsResource {

    @POST
    void create();

}
