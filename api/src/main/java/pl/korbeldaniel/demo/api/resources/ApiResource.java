package pl.korbeldaniel.demo.api.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/") public interface ApiResource {

    @GET @Path("ping") String ping();
    @GET @Path("pingJwtSecured") String pingJwtSecured();

}
