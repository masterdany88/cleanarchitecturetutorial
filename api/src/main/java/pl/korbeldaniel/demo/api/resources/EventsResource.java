package pl.korbeldaniel.demo.api.resources;

import pl.korbeldaniel.demo.domain.model.OrganizerDto;
import pl.korbeldaniel.demo.domain.model.event.EventDto;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Collection;
import java.util.List;

@Path("events")
@Produces(MediaType.APPLICATION_JSON)
public interface EventsResource {

    @GET
    List<EventDto> getAll();

    @GET
    @Path("summary/{organizer}")
    List<EventDto> getSummaryOfAllActiveByOrganizer(@PathParam("organizer") OrganizerDto organizer);

    @GET
    @Path("{id}/summary")
    EventDto getSummaryById(@PathParam("id") Long id);

    @GET
    @Path("{id}")
    EventDto getById(@PathParam("id") Long id);

    @GET
    @Path("{id}/{ticketCode}")
    EventDto getByIdAndTicketCode(@PathParam("id") Long id, @PathParam("ticketCode") String ticketCode);

    @Path("{id}/pageContent/{ticketCode}")
    @Produces(MediaType.TEXT_PLAIN)
    String getPageContentById(@PathParam("id") Long id, @PathParam("ticketCode") String ticketCode);

    @POST
    EventDto createNew(EventDto dto);

    @PUT
    @Path("{id}")
    void updateById(@PathParam("id") Long id, EventDto dto);

    @Path("{eventId}/files")
    EventFilesResources get(@PathParam("eventId") String eventId);
}
