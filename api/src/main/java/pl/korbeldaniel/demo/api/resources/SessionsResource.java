package pl.korbeldaniel.demo.api.resources;

import pl.korbeldaniel.demo.domain.model.AdminSessionDto;
import pl.korbeldaniel.demo.domain.model.ResponseDto;
import pl.korbeldaniel.demo.domain.model.StreamingSessionDto;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("sessions")
@Produces(MediaType.APPLICATION_JSON)
public interface SessionsResource {

    @POST
    StreamingSessionDto createSession(StreamingSessionDto streamingSessionDto);

    @POST
    @Path("admin")
    String createSession(AdminSessionDto adminSessionDto);

    @GET
    @Path("{id}/{accessCode}")
    ResponseDto getSessionValidity(@PathParam("id") String id, @PathParam("accessCode") String accessCode);
}
