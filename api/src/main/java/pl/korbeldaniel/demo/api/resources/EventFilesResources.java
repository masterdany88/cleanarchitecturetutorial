package pl.korbeldaniel.demo.api.resources;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.io.InputStream;

public interface EventFilesResources {

    @POST
    @Path("tickets")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    void uploadTicketsFile(InputStream tickets) throws IOException;

}
