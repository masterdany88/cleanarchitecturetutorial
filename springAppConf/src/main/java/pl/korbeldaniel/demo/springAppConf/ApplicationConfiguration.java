package pl.korbeldaniel.demo.springAppConf;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.thymeleaf.TemplateEngine;
import pl.korbeldaniel.demo.email.*;
import pl.korbeldaniel.demo.payment.ApiGateway;
import pl.korbeldaniel.demo.repository.event.EventDaoImpl;
import pl.korbeldaniel.demo.repository.event.EventRepository;
import pl.korbeldaniel.demo.repository.session.StreamingSessionDaoImpl;
import pl.korbeldaniel.demo.repository.session.StreamingSessionRepository;
import pl.korbeldaniel.demo.repository.ticket.TicketCodeDaoImpl;
import pl.korbeldaniel.demo.repository.ticket.TicketCodeRepository;
import pl.korbeldaniel.demo.repository.user.UserDaoImpl;
import pl.korbeldaniel.demo.repository.user.UserRepository;
import pl.korbeldaniel.demo.usecase.SendEmailUc;
import pl.korbeldaniel.demo.usecase.buy.GetPaymentMethodsUc;
import pl.korbeldaniel.demo.usecase.buy.PaymentPort;
import pl.korbeldaniel.demo.usecase.event.CreateEventUc;
import pl.korbeldaniel.demo.usecase.event.UpdateEventUc;
import pl.korbeldaniel.demo.usecase.port.dao.*;
import pl.korbeldaniel.demo.usecase.port.email.EmailServicePort;
import pl.korbeldaniel.demo.usecase.ticket.CreateTicketCodesUc;
import pl.korbeldaniel.demo.usecase.ticket.FilterTicketCodesUc;
import pl.korbeldaniel.demo.usecase.event.FindEventsUc;
import pl.korbeldaniel.demo.usecase.user.CreateSessionUc;
import pl.korbeldaniel.demo.usecase.user.CreateUserUc;
import pl.korbeldaniel.demo.usecase.user.FindUsersUc;
import pl.korbeldaniel.demo.usecase.user.GetSessionValidityUc;

@Configuration
@EntityScan("pl.korbeldaniel.demo")
@ComponentScan("pl.korbeldaniel.demo")
public class ApplicationConfiguration {
    @Bean public UserDao userDao(UserRepository repository) {
        return new UserDaoImpl(repository);
    }
    @Bean public EventDao eventDao(EventRepository repository) { return new EventDaoImpl(repository);}
    @Bean public TicketCodeDao ticketCodeDao(TicketCodeRepository ticketCodeRepository, EventRepository eventRepository) {return new TicketCodeDaoImpl(ticketCodeRepository, eventRepository);}
    @Bean public StreamingSessionDao streamingSessionDao(StreamingSessionRepository streamingSessionRepository, TicketCodeRepository ticketCodeRepository) {return new StreamingSessionDaoImpl(streamingSessionRepository, ticketCodeRepository);}
    @Bean public FindEventsUc findEventsUc(EventDao eventDao, TicketCodeDao ticketCodeDao) {return new FindEventsUc(eventDao, ticketCodeDao);}
    @Bean public CreateEventUc createEventUc(EventDao eventDao) {return new CreateEventUc(eventDao);}
    @Bean public UpdateEventUc updateEventUc(EventDao eventDao) {return new UpdateEventUc(eventDao);}
    @Bean public CreateUserUc createUserUc(UserDao userDao) {return new CreateUserUc(userDao);}
    @Bean public FindUsersUc findUserUc(UserDao userDao) {return new FindUsersUc(userDao);}
    @Bean public CreateTicketCodesUc createNewTicketCodesUc(TicketCodeDao ticketCodeDao) {return new CreateTicketCodesUc(ticketCodeDao);}
    @Bean public FilterTicketCodesUc filterNewTicketCodesUc() {return new FilterTicketCodesUc();}

    @Bean public CreateSessionUc createSessionU(StreamingSessionDao streamingSessionDao, TicketCodeDao ticketCodeDao, EventDao eventDao) {return new CreateSessionUc(streamingSessionDao, ticketCodeDao, eventDao);}
    @Bean public GetSessionValidityUc getSessionValidityUc(StreamingSessionDao streamingSessionDao) { return new GetSessionValidityUc(streamingSessionDao); }

    @Bean public EmailServicePort emailService(JavaMailSender javaMailSender, TemplateEngine templateEngine) { return new EmailServiceAdapter(javaMailSender, templateEngine);}
    @Bean public SendEmailUc sendEmailUc(EmailServiceAdapter emailServiceAdapter) {return new SendEmailUc(emailServiceAdapter);}
    @Bean public PaymentPort paymentPort() {return new ApiGateway();}
    @Bean public GetPaymentMethodsUc getPaymentMethodsUc(PaymentPort paymentPort) {return new GetPaymentMethodsUc(paymentPort);}
}
