package pl.korbeldaniel.demo.server.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.core.env.Environment;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.util.logging.Logger;

@Provider
@JWTTokenNeeded
@Priority(Priorities.AUTHENTICATION)
public class JWTTokenNeededFilter implements ContainerRequestFilter {
    private String secret;
    @Inject
    private Environment env;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        secret = env.getProperty("jwt.secret");
        Logger logger = Logger.getLogger(JWTTokenNeededFilter.class.getCanonicalName());
        // Get the HTTP Authorization header from the request
        String authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
        // Extract the token from the HTTP Authorization header
        if((authorizationHeader == null) || ("".equals(authorizationHeader))) {
            logger.severe("#### empty authorization header");
            requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
        } else {
            String token = authorizationHeader.substring("Bearer".length()).trim();
            try {
                // Validate the token
//            Key key = keyGenerator.generateKey();
//            Jwts.parser().setSigningKey(key).parseClaimsJws(token);
                decodeJWT(token);
                logger.info("#### valid token : " + token);
            } catch (Exception e) {
                logger.severe("#### invalid token : " + token + ". " + e.getMessage());
                requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
            }
        }
    }
    public Claims decodeJWT(String jwt) {
        //This line will throw an exception if it is not a signed JWS (as expected)
        Claims claims = Jwts.parser()
                .setSigningKey(DatatypeConverter.parseBase64Binary(secret))
                .parseClaimsJws(jwt).getBody();
        return claims;
    }
}