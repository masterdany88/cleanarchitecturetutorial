package pl.korbeldaniel.demo.server.resources;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import pl.korbeldaniel.demo.api.resources.UserRolesResource;
import pl.korbeldaniel.demo.api.resources.UsersResource;
import pl.korbeldaniel.demo.domain.model.UserDto;
import pl.korbeldaniel.demo.usecase.user.CreateUserUc;
import pl.korbeldaniel.demo.usecase.user.FindUsersUc;

import java.util.Collection;

@Component
@Scope("request")
public class UsersRestController implements UsersResource {
    private final CreateUserUc createUserUc;
    private final FindUsersUc findUsersUc;
    private final UserRolesRestController userRolesRestController;

    public UsersRestController(CreateUserUc createUserUc, FindUsersUc findUsersUc, UserRolesRestController userRolesRestController) {
        this.createUserUc = createUserUc;
        this.findUsersUc = findUsersUc;
        this.userRolesRestController = userRolesRestController;
    }

    @Override public Collection<UserDto> getUsers() {
        return findUsersUc.findAll();
    }

    @Override public UserDto getUser(Long id) {
        return findUsersUc.findById(id).get();
    }

    @Override public UserDto createNewUser(UserDto userDto) {
        return createUserUc.createUser(userDto);
    }

    @Override
    public void update(Long userId, UserDto userDto) {
        System.out.println("updated userId = " + userId + ", userDto = " + userDto);
    }

    @Override
    public UserDto getTestUser() {
        return new UserDto(55L, "Test user name", 100);
    }

    @Override public UserRolesResource getUserRolesResource(Long id) {
        return userRolesRestController.withUserId(id);
    }

}
