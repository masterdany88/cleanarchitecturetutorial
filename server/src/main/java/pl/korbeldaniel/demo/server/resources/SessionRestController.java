package pl.korbeldaniel.demo.server.resources;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import pl.korbeldaniel.demo.api.resources.SessionsResource;
import pl.korbeldaniel.demo.domain.model.AdminSessionDto;
import pl.korbeldaniel.demo.domain.model.ResponseDto;
import pl.korbeldaniel.demo.domain.model.StreamingSessionDto;
import pl.korbeldaniel.demo.usecase.user.CreateSessionUc;
import pl.korbeldaniel.demo.usecase.user.GetSessionValidityUc;

import javax.crypto.spec.SecretKeySpec;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;

import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;

@Component
@Scope("request")
public class SessionRestController implements SessionsResource {
    private final CreateSessionUc createSessionUc;
    private final GetSessionValidityUc getSessionValidityUc;
    private final String secret;
    @Context private HttpHeaders headers;
    @Context private UriInfo uriInfo;

    public SessionRestController(@Value("${jwt.secret}") final String secret, CreateSessionUc createSessionUc, GetSessionValidityUc getSessionValidityUc) {
        this.secret = secret;
        this.createSessionUc = createSessionUc;
        this.getSessionValidityUc = getSessionValidityUc;
    }

    @Override
    public StreamingSessionDto createSession(StreamingSessionDto streamingSessionDto) {
//        streamingSessionDto.getTicketCode().setEventId(eventDao.findCurrentEventId().get());
        try {
            return createSessionUc.createSession(streamingSessionDto, headers.getHeaderString("host"));
        } catch (IllegalArgumentException e) {
            throw new NotAuthorizedException("Kod dostępu jest niepoprawny");
        }
    }

    public String createSession(AdminSessionDto adminSessionDto) {
        try {
            // Authenticate the user using the credentials provided
            authenticate(adminSessionDto.getLogin(), adminSessionDto.getPassword());
            // Issue a token for the user
            String token = issueToken(uriInfo, adminSessionDto.getLogin());
            // Return the token on the response
//            return Response.ok().header(AUTHORIZATION, "Bearer " + token).build();
            return token;
        } catch (Exception e) {
            //return Response.status(UNAUTHORIZED).build();
            throw new WebApplicationException(UNAUTHORIZED);
        }
    }

    private void authenticate(String login, String password) throws Exception {
//        TypedQuery<User> query = em.createNamedQuery(User.FIND_BY_LOGIN_PASSWORD, User.class);
//        query.setParameter("login", login);
//        query.setParameter("password", PasswordUtils.digestPassword(password));
//        User user = query.getSingleResult();
//
        if ("admin".equals(login)
                && "supertrudnetymczasowehaslo12345!@#$%ZXCV".equals(password)) {
            //ok
        } else {
            throw new SecurityException("Invalid user/password");
        }
//        if (user == null)
//            throw new SecurityException("Invalid user/password");
    }

//    private String issueToken(String login) {
//        Key key = keyGenerator.generateKey();
//        String jwtToken = Jwts.builder()
//                .setSubject(login)
//                .setIssuer(uriInfo.getAbsolutePath().toString())
//                .setIssuedAt(new Date())
//                .setExpiration(toDate(LocalDateTime.now().plusMinutes(15L)))
//                .signWith(SignatureAlgorithm.HS512, key)
//                .compact();
//        return jwtToken;
//    }

    // The secret key. This should be in a property file NOT under source
// control and not hard coded in real life. We're putting it here for
// simplicity.
    int jwtTimeToLive = 800000;

    private String issueToken(UriInfo uriInfo, String login) {

        //The JWT signature algorithm we will be using to sign the token
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        //We will sign our JWT with our ApiKey secret
        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(secret);
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

        //Let's set the JWT Claims
        JwtBuilder builder = Jwts.builder().setId("SOMEID123456789")
                .setIssuedAt(now)
                .setSubject(login)
                .setIssuer(uriInfo.getAbsolutePath().toString())
                .signWith(signatureAlgorithm, signingKey);

        //if it has been specified, let's add the expiration
        if (jwtTimeToLive > 0) {
            long expMillis = nowMillis + jwtTimeToLive;
            Date exp = new Date(expMillis);
            builder.setExpiration(exp);
        }

        //Builds the JWT and serializes it to a compact, URL-safe string
        String jwt = builder.compact();
        return jwt;
    }

    @Override
    public ResponseDto getSessionValidity(String id, String accessCode) {
        return getSessionValidityUc.get(id, accessCode);
    }

    private Date toDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }
}
