package pl.korbeldaniel.demo.server.resources;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import pl.korbeldaniel.demo.api.resources.UserRolesResource;

@Component
@Scope("request")
public class UserRolesRestController implements UserRolesResource {
    private Long userId;

    @Override public String getAll() {
        return userId + "=r1, r2, r3";
    }

    @Override public String getById(Long id) {
        return "userId " + userId + "roleId=" + id;
    }

    UserRolesResource withUserId(Long userId) {
        this.userId = userId;
        return this;
    }
}
