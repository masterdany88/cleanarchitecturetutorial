package pl.korbeldaniel.demo.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Properties;

@EnableSwagger2
@SpringBootApplication
@ComponentScan("pl.korbeldaniel.demo")
public class DemoApplication  extends SpringBootServletInitializer {
	public static void main(String[] args) {
		SpringApplication application = new SpringApplication(DemoApplication.class);
		Properties properties = new Properties();
		properties.put("spring.jersey.type", "filter");
		application.setDefaultProperties(properties);
		application.run(args);
	}
}
