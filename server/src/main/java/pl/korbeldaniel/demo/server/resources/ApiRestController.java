package pl.korbeldaniel.demo.server.resources;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import pl.korbeldaniel.demo.api.resources.ApiResource;
import pl.korbeldaniel.demo.server.security.JWTTokenNeeded;

@Component
@Scope("request")
public class ApiRestController implements ApiResource {
    @Override
    public String ping() {
        return "pong";
    }

    @Override
    @JWTTokenNeeded
    public String pingJwtSecured() {
        return "pongJwtSecured";
    }

}
