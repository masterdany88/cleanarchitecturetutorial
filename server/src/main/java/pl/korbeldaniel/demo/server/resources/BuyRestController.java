package pl.korbeldaniel.demo.server.resources;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import pl.korbeldaniel.demo.api.resources.BuyResource;
import pl.korbeldaniel.demo.domain.model.payment.PaymentMethodsResponse;
import pl.korbeldaniel.demo.usecase.buy.GetPaymentMethodsUc;

@Component
@Scope("request")
public class BuyRestController implements BuyResource {
    private final GetPaymentMethodsUc getPaymentMethodsUc;

    public BuyRestController(GetPaymentMethodsUc getPaymentMethodsUc) {
        this.getPaymentMethodsUc = getPaymentMethodsUc;
    }

    @Override
    public PaymentMethodsResponse getPaymentsMethods() {
        return getPaymentMethodsUc.getPaymentMethods();
    }
}
