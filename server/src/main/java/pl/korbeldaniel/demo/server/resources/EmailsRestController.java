package pl.korbeldaniel.demo.server.resources;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import pl.korbeldaniel.demo.api.resources.EmailsResource;
import pl.korbeldaniel.demo.usecase.port.email.EmailServicePort;

@Component
@Scope("request")
public class EmailsRestController implements EmailsResource {
    private final EmailServicePort emailServicePort;

    public EmailsRestController(EmailServicePort emailServicePort) {
        this.emailServicePort = emailServicePort;
    }

    @Override
//    @JWTTokenNeeded
    public void create() {
        emailServicePort.sendTicketPurchasedConfirmationEmail("masterdany88@gmail.com", "Test email", "Event name", "access code dskljfaoie3k2jlkj32 32 ");
    }
}
