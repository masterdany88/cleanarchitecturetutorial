package pl.korbeldaniel.demo.server.resources;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import pl.korbeldaniel.demo.api.resources.EventFilesResources;
import pl.korbeldaniel.demo.api.resources.EventsResource;
import pl.korbeldaniel.demo.domain.model.OrganizerDto;
import pl.korbeldaniel.demo.domain.model.event.EventDto;
import pl.korbeldaniel.demo.server.security.JWTTokenNeeded;
import pl.korbeldaniel.demo.usecase.event.CreateEventUc;
import pl.korbeldaniel.demo.usecase.event.FindEventsUc;
import pl.korbeldaniel.demo.usecase.event.UpdateEventUc;

import java.util.List;

@Component
@Scope("request")
public class EventsRestController implements EventsResource {
    private final FindEventsUc findEventsUc;
    private final CreateEventUc createEventUc;
    private final UpdateEventUc updateEventUc;
    private final EventFilesRestController eventFilesRestController;

    public EventsRestController(FindEventsUc findEventsUc, CreateEventUc createEventUc, UpdateEventUc updateEventUc, EventFilesRestController eventFilesRestController) {
        this.findEventsUc = findEventsUc;
        this.createEventUc = createEventUc;
        this.updateEventUc = updateEventUc;
        this.eventFilesRestController = eventFilesRestController;
    }

    @Override
    @JWTTokenNeeded
    public List<EventDto> getAll() {
        return findEventsUc.findAll();
    }

    @Override
    public List<EventDto> getSummaryOfAllActiveByOrganizer(OrganizerDto organizer) {
        return findEventsUc.findSummaryOfAllActiveByOrganizer(organizer);
    }

    @Override
    public EventDto getSummaryById(Long id) {
        return findEventsUc.findSummaryById(id);
    }

    @Override
    @JWTTokenNeeded
    public EventDto getById(Long id) {
        return findEventsUc.findById(id).get();
    }

    @Override
    public EventDto getByIdAndTicketCode(Long id, String ticketCode) {
        return findEventsUc.findByIdAndTicketCode(id, ticketCode).get();
    }

    @Override
    public String getPageContentById(Long id, String ticketCode) {
        return findEventsUc.findByIdAndTicketCode(id, ticketCode).get().getPageContent();
    }

    @Override
    @JWTTokenNeeded
    public EventDto createNew(EventDto dto) {
        return createEventUc.create(dto);
    }

    @Override
    @JWTTokenNeeded
    public void updateById(Long id, EventDto dto) {
        updateEventUc.update(dto);
    }

    @Override
    public EventFilesResources get(String eventId) {
        return eventFilesRestController.withEventId(eventId);
    }
}
