package pl.korbeldaniel.demo.server.resources;

import com.opencsv.CSVReader;
import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import pl.korbeldaniel.demo.api.resources.EventFilesResources;
import pl.korbeldaniel.demo.usecase.port.dao.EventDao;
import pl.korbeldaniel.demo.domain.model.ticket.TicketCodeDto;
import pl.korbeldaniel.demo.server.security.JWTTokenNeeded;
import pl.korbeldaniel.demo.usecase.ticket.CreateTicketCodesUc;
import pl.korbeldaniel.demo.usecase.ticket.FilterTicketCodesUc;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.io.*;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@Scope("request")
public class EventFilesRestController implements EventFilesResources {
    private final FilterTicketCodesUc filterTicketCodesUc;
    private final CreateTicketCodesUc createTicketCodesUc;
    private String eventId;
    private EventDao eventDao;
    private static final String CURRENT_EVENT_ID = "current";

    public EventFilesRestController(FilterTicketCodesUc filterTicketCodesUc, CreateTicketCodesUc createTicketCodesUc, EventDao eventDao) {
        this.filterTicketCodesUc = filterTicketCodesUc;
        this.createTicketCodesUc = createTicketCodesUc;
        this.eventDao = eventDao;
    }

    @JWTTokenNeeded
    public void uploadTicketsFile(InputStream ticketsCodes) throws IOException {
        InputStreamReader inputStreamReader = new InputStreamReader(ticketsCodes);
        CSVReader reader = new CSVReader(inputStreamReader);
        List<String[]> codes = reader.readAll();
        reader.close();
        inputStreamReader.close();
        List<String> filteredCodes = filterTicketCodesUc.filter(codes);
        createTicketCodesUc.createTickets(
                filteredCodes.stream().map(codeId -> new TicketCodeDto(eventId, codeId)).collect(Collectors.toList()),
                Long.valueOf(eventId)
        );
    }

    @POST
    @Path("/topBanner")
    @JWTTokenNeeded
    public Response uploadTopBannerFile(
                               @FormDataParam("topBanner.png")InputStream uploadedInputStream,
                               @FormDataParam("topBanner.png") FormDataContentDisposition fileDetail) throws IOException {
        byte[] targetArray = IOUtils.toByteArray(uploadedInputStream);
        eventDao.updateTopBannerById(targetArray, Long.valueOf(eventId));
        return Response.status(201).entity("File has been successfully stored").build();
    }

    @POST
    @Path("/cover")
    @JWTTokenNeeded
    public Response uploadCoverFile(
                               @FormDataParam("cover.png") InputStream uploadedInputStream,
                               @FormDataParam("cover.png") FormDataContentDisposition fileDetail) throws IOException {
        byte[] targetArray = IOUtils.toByteArray(uploadedInputStream);
        eventDao.updateCoverById(targetArray, Long.valueOf(eventId));
        return Response.status(201).entity("File has been successfully stored").build();
    }
//
//    // save uploaded file to new location
//    private void writeToFile(InputStream uploadedInputStream,
//                             String uploadedFileLocation) {
//
//        try {
//            OutputStream out = new FileOutputStream(new File(
//                    uploadedFileLocation));
//            int read = 0;
//            byte[] bytes = new byte[1024];
//
//            out = new FileOutputStream(new File(uploadedFileLocation));
//            while ((read = uploadedInputStream.read(bytes)) != -1) {
//                out.write(bytes, 0, read);
//            }
//            out.flush();
//            out.close();
//        } catch (IOException e) {
//
//            e.printStackTrace();
//        }
//
//    }
//        String uploadedFileLocation = "/home/daniel/AbbAAAAAAAAfileDestnew_1.png" ;
//        File targetFile = new File(uploadedFileLocation);
//        OutputStream outStream = new FileOutputStream(targetFile);
//        BufferedReader reader = new BufferedReader(new InputStreamReader(file));
//        String line = "";
//        String output = "";
//        while(reader.ready()) {
//            line = reader.readLine();
//            if(line.contains("------WebKitFormBoundary")
//            || line.contains("Content-Disposition:")
//            || line.contains("Content-Type:")
//            ) {
//                // ignore
//            } else {
//                outStream.write(line.getBytes());
//            }
//        }
//
//
////
////        byte[] buffer = new byte[8 * 1024];
////        int bytesRead;
////        while ((bytesRead = initialStream.read(buffer)) != -1) {
////            outStream.write(buffer, 0, bytesRead);
////        }
//        IOUtils.closeQuietly(reader);
//        IOUtils.closeQuietly(file);
//        IOUtils.closeQuietly(outStream);
//
//        return Response.status(200).entity("s").build();
//
//    }


    @GET
    @javax.ws.rs.Path("topBanner")
    @Produces("image/png")
    public Response getTopBannerFile(@Context HttpHeaders headers) {
        if(CURRENT_EVENT_ID.equals(eventId)) {
            Optional<Long> currentEventId = eventDao.findCurrentEventIdByWwwDomain(headers.getHeaderString("host"));
//            Optional<Long> currentEventId = eventDao.findCurrentEventIdByWwwDomain(headers.getHeaderString("host"));
            eventId = String.valueOf(currentEventId.get());
//            eventId = eventDao.findCurrentEvent().get().getId().toString();
        }
        byte[] image = eventDao.findTopBannerById(Long.valueOf(eventId));
        return Response.ok().entity((StreamingOutput) output -> {
            output.write(image);
            output.flush();
        }).build();
    }

    @GET
    @javax.ws.rs.Path("cover")
    @Produces("image/png")
    public Response getCoverFile(@Context HttpHeaders headers) {
        if(CURRENT_EVENT_ID.equals(eventId)) {
            Optional<Long> currentEventId = eventDao.findCurrentEventIdByWwwDomain(headers.getHeaderString("host"));
            eventId = String.valueOf(currentEventId.get());
//            eventId = eventDao.findCurrentEvent().get().getId().toString();
//            eventId = eventDao.findCurrentEvent().get().getId().toString();
        }
        byte[] image = eventDao.findCoverById(Long.valueOf(eventId));
        return Response.ok().entity((StreamingOutput) output -> {
            output.write(image);
            output.flush();
        }).build();
    }

    EventFilesResources withEventId(String eventId) {
        this.eventId = eventId;
        return this;
    }
}
