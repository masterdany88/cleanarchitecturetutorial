package pl.korbeldaniel.demo.client.ui.streaming.content.events;

import com.github.nalukit.nalu.client.component.AbstractComponent;
import elemental2.dom.DomGlobal;
import elemental2.dom.HTMLDivElement;
import elemental2.dom.HTMLElement;
import org.dominokit.domino.ui.button.Button;
import org.dominokit.domino.ui.button.DropdownButton;
import org.dominokit.domino.ui.button.group.ButtonsGroup;
import org.dominokit.domino.ui.cards.Card;
import org.dominokit.domino.ui.dropdown.DropdownAction;
import org.dominokit.domino.ui.grid.Column;
import org.dominokit.domino.ui.grid.Row;
import org.dominokit.domino.ui.grid.flex.*;
import org.dominokit.domino.ui.style.Color;
import org.dominokit.domino.ui.utils.DominoElement;
import org.gwtproject.i18n.shared.DateTimeFormat;
import pl.korbeldaniel.demo.client.widget.MyColor;
import pl.korbeldaniel.demo.domain.model.event.EventDto;

import java.util.List;

import static org.jboss.elemento.Elements.h;
import static org.jboss.elemento.Elements.img;

public class EventsComponent extends AbstractComponent<IEventsComponent.Controller, HTMLElement> implements IEventsComponent {
    private DominoElement<HTMLDivElement> root = DominoElement.div();

    @Override
    public void render() {
        root.appendChild(Row.create()
                .addColumn(Column.span12().appendChild(
                        h(2).css("standup").textContent("WYDARZENIA")
                )));
        initElement(root.element());
    }

    @Override
    public void display(List<EventDto> events) {
        FlexLayout flexLayout =
                FlexLayout.create()
                        .style()
//                        .add("demo-flex-layout-container")
                        .get()
                        .setWrap(FlexWrap.WRAP_TOP_TO_BOTTOM)
                        .setJustifyContent(FlexJustifyContent.CENTER)
                        .setDirection(FlexDirection.LEFT_TO_RIGHT);
        events.forEach(e -> {
            String dateTime = "";
            if (e.getDate() != null && e.getTime() != null) {
                DateTimeFormat dateFormat = DateTimeFormat.getFormat("yyyy-MM-dd");
                String textDate = dateFormat.format(e.getDate());
                DateTimeFormat timeFormat = DateTimeFormat.getFormat("HH:mm");
                String textTime = timeFormat.format(e.getTime());
                dateTime = textDate + " " + textTime;
            }

//            Row_12 buttonsRow = Row.create();
            Button watchButton = Button.create("OGLĄDAJ")
                    .setBackground(MyColor.STANDUP_YELLOW)
                    .addClickListener(ev -> getController().routeToEventStream(e.getId()));
//            buttonsRow.addColumn(Column.span6().appendChild(
//                    watchButton
//                    )
//            );
            ButtonsGroup buttonGroup = ButtonsGroup.create();
            flexLayout.appendChild(
                    FlexItem.create().css("eventFlexCard").appendChild(
                            Card.create()
                                    .setBackground(Color.BLACK)
                                    .appendChild(img(DomGlobal.location.getOrigin() + "/api/events/" + e.getId() + "/files/cover").css("img_h"))
                                    .appendChild(h(3).textContent(e.getName()))
                                    .appendChild(h(3).textContent(dateTime))
                                    .appendChild(buttonGroup)));
            buttonGroup.appendChild(watchButton);

            if (!e.getBuyTicketUrls().isEmpty()) {
                DropdownButton buyTicketButton = DropdownButton.create("KUP BILET");
                buyTicketButton.setBackground(MyColor.STANDUP_YELLOW);
                buttonGroup.appendChild(buyTicketButton);
                e.getBuyTicketUrls().forEach(url -> {
                    buyTicketButton.appendChild(DropdownAction
                            .create(url.getName())
                            .addClickListener(evt -> DomGlobal.window.open(url.getUrl())));
                });
            }

//            if (e.getTicketsUrl() != null && !e.getTicketsUrl().equals("")) {
//                                .appendChild(watchButton)
//                Button buyTicketButton = Button.create("KUP BILET")
//                        .setBackground(MyColor.STANDUP_YELLOW)
//                        .addClickListener(ev -> DomGlobal.window.open(e.getTicketsUrl(), "Kup bilet"));
//                                                        .addClickListener(ev -> getController().showBuyTicketPopUp(e.getId()))
//            buttonGroup.appendChild(buyTicketButton);
//            }
        });
        root.appendChild(flexLayout);
    }
}
