package pl.korbeldaniel.demo.client.ui.streaming.content.faq;

import com.github.nalukit.nalu.client.component.AbstractComponentController;
import com.github.nalukit.nalu.client.component.annotation.Controller;
import elemental2.dom.HTMLElement;
import pl.korbeldaniel.demo.client.Routes;
import pl.korbeldaniel.demo.client.UiContext;

@Controller(
        route = Routes.FAQ,
        selector = "content",
        componentInterface = IFaqComponent.class,
        component = FaqComponent.class
)
public class FaqController extends AbstractComponentController<UiContext, IFaqComponent, HTMLElement> implements IFaqComponent.Controller {

}
