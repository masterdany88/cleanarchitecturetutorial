package pl.korbeldaniel.demo.client.ui.admin.content.events;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import de.knightsoftnet.validators.client.editor.BeanValidationEditorDriver;
import elemental2.dom.DomGlobal;
import elemental2.dom.HTMLDivElement;
import elemental2.dom.HTMLElement;
import org.dominokit.domino.ui.button.Button;
import org.dominokit.domino.ui.forms.FieldsGrouping;
import org.dominokit.domino.ui.forms.IntegerBox;
import org.dominokit.domino.ui.forms.TextBox;
import org.dominokit.domino.ui.forms.validations.ValidationResult;
import org.dominokit.domino.ui.grid.Column;
import org.dominokit.domino.ui.grid.Row;
import org.dominokit.domino.ui.grid.Row_12;
import org.dominokit.domino.ui.notifications.Notification;
import org.dominokit.domino.ui.utils.DominoElement;
import org.jboss.elemento.IsElement;
import pl.korbeldaniel.demo.domain.model.event.BuyTicketUrlDto;

import static org.jboss.elemento.Elements.div;

public class BuyTicketUrlEditor implements Editor<BuyTicketUrlDto>, IsElement<HTMLElement> {

    private BuyTicketUrlEditor.Driver driver = GWT.create(BuyTicketUrlEditor.Driver.class);
    interface Driver extends BeanValidationEditorDriver<BuyTicketUrlDto, BuyTicketUrlEditor> {}
    private FieldsGrouping fieldsGrouping = FieldsGrouping.create();

    private DominoElement<HTMLDivElement> rootElement;
    private TextBox id;
    private TextBox url;
    private TextBox name;
    private IntegerBox order;
    private DominoElement<HTMLDivElement> actionButtonContainer;
    private Button updateButton;
    private Button deleteButton;


    static BuyTicketUrlEditor create(BuyTicketUrlDto dto) {
        BuyTicketUrlEditor editor = new BuyTicketUrlEditor();
        DomGlobal.console.log(dto.getName() + " " + dto.getUrl() + " " + dto.getOrder() + " " + dto.getId());
        editor.edit(dto);
        return editor;
    }

    private BuyTicketUrlEditor() {
        rootElement = DominoElement.of(div());
        Row_12 row2 = Row.create();
        this.rootElement.appendChild(row2);
        id = TextBox.create().hide();
        url = TextBox.create("Url do zakupu biletów");
        name = TextBox.create("Nazwa");
        order = IntegerBox.create("Kolejność");
        actionButtonContainer = DominoElement.of(div());
        updateButton = Button.createInfo("Aktualizuj/ Dodaj");
        deleteButton = Button.createDanger("Usuń");
        row2.addColumn(Column.span5().appendChild(url));
        row2.addColumn(Column.span4().appendChild(name));
        row2.addColumn(Column.span1().appendChild(order));
        row2.addColumn(Column.span2().appendChild(actionButtonContainer));
        actionButtonContainer.appendChild(deleteButton);
        setUpChangeHandler();
    }

    private void setUpChangeHandler() {
        id.addChangeHandler(value -> changeFormToUpdate());
        url.addChangeHandler(value -> changeFormToUpdate());
        name.addChangeHandler(value -> changeFormToUpdate());
        order.addChangeHandler(value -> changeFormToUpdate());
    }

    private void changeFormToUpdate() {
        actionButtonContainer.clearElement();
        actionButtonContainer.appendChild(updateButton);
    }

    @Override
    public HTMLElement element() {
        return rootElement.element();
    }

    void edit(BuyTicketUrlDto dto) {
        driver.initialize(this);
        driver.edit(dto);
        if(dto.getId() == null) {
            deleteButton.hide();
        } else {
            deleteButton.show();
            id.setValue(dto.getId().toString());
        }
        url.setValue(dto.getUrl());
        name.setValue(dto.getName());
        order.setValue(dto.getOrder());
    }

    public BuyTicketUrlDto save() {
        BuyTicketUrlDto dto;
        if (fieldsGrouping.validate().isValid()) {
            dto = driver.flush();
            if (driver.hasErrors()) {
                Notification.createDanger("Fail to update " + driver.getErrors()).show();
            } else {
                //getController().persistUser(updatedUser);
//                onSaveHandler.accept(dto);
                return dto;
            }
        } else {
            throw new IllegalArgumentException();
        }
        return dto;
    }
}
