package pl.korbeldaniel.demo.client.widget;

import org.dominokit.domino.ui.notifications.Notification;

public class UiNotifier {

    public void showInfo(String message) {
        Notification.createInfo(message).show();
    }

    public void showSuccess(String message) {
        Notification.createSuccess(message).show();
    }

    public void showWarning(String message) {
        Notification.createWarning(message).show();
    }

    public void showError(String message) {
        Notification.createDanger(message).show();
    }
}
