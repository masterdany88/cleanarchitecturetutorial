package pl.korbeldaniel.demo.client.ui.streaming.content.contact;

import com.github.nalukit.nalu.client.component.AbstractComponent;
import elemental2.dom.HTMLDivElement;
import elemental2.dom.HTMLElement;
import org.dominokit.domino.ui.Typography.Paragraph;
import org.dominokit.domino.ui.grid.Column;
import org.dominokit.domino.ui.grid.Row;
import org.dominokit.domino.ui.utils.DominoElement;

import static org.jboss.elemento.Elements.h;
import static org.jboss.elemento.Elements.img;

public class ContactComponent extends AbstractComponent<IContactComponent.Controller, HTMLElement> implements IContactComponent {
    private DominoElement<HTMLDivElement> root = DominoElement.div();

    @Override
    public void render() {
        root.appendChild(Row.create()
                .addColumn(Column.span12().appendChild(
                        h(2).css("standup").textContent("KONTAKT")
                )));
        root.appendChild(Row.create()
                        .addColumn(Column.span3())
                        .addColumn(Column.span6()
                                .appendChild(img("img/agencja_standup_comedy.png").css("img_h"))
                                .appendChild((h(4).textContent("Kontakt mailowy do reklamacji/ pomocy technicznej:").css("center")))
                                .appendChild((h(4).textContent("kontakt@standupnolimits.pl").css("center")))
                                .appendChild((h(4).textContent("Kontakt do współpracy:").css("center")))
                                .appendChild((h(4).textContent("Agencja Stand-up Comedy Bartłomiej Korbel").css("center")))
                                .appendChild((h(4).textContent("Henryków 12, 97 - 217 Lubochnia").css("center")))
                                .appendChild((h(4).textContent("NIP: 773 240 71 30").css("center")))
                                .appendChild((h(4).textContent("kontakt@standupnolimits.pl").css("center").css("center")))
                                .appendChild((h(4).textContent("telefon: 503 825 640").css("center")))
                        )
        );
        initElement(root.element());
    }

}
