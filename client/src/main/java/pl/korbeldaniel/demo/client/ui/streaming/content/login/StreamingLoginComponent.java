package pl.korbeldaniel.demo.client.ui.streaming.content.login;

import com.github.nalukit.nalu.client.component.AbstractComponent;
import elemental2.dom.*;
import org.dominokit.domino.ui.button.Button;
import org.dominokit.domino.ui.button.ButtonSize;
import org.dominokit.domino.ui.forms.FieldStyle;
import org.dominokit.domino.ui.forms.TextBox;
import org.dominokit.domino.ui.grid.Column;
import org.dominokit.domino.ui.grid.Row;
import org.dominokit.domino.ui.keyboard.KeyboardEvents;
import pl.korbeldaniel.demo.client.widget.Window;
import pl.korbeldaniel.demo.domain.model.event.BuyTicketUrlDto;

import java.util.List;

import static org.dominokit.domino.ui.keyboard.KeyboardEvents.ENTER;
import static org.jboss.elemento.Elements.*;
import static org.jboss.elemento.EventType.click;

public class StreamingLoginComponent extends AbstractComponent<IStreamingLoginComponent.Controller, HTMLElement> implements IStreamingLoginComponent {
    private TextBox ticketCodeInput;
    private Button submitButton;
    private HTMLDivElement root = div().element();

    @Override
    public void display(String eventId, List<BuyTicketUrlDto> ticketUrls) {
        Row ticketsRow = Row.create();
        this.root.appendChild(
                Row.create()
                        .addColumn(Column.span12().appendChild(
                                h(2).css("standup").textContent("WYDARZENIE")
                        )).element()
        );
//                        .addColumn(Column.span12().appendChild(
//                                img(DomGlobal.location.getOrigin() + "/api/events/"+ eventId +"/files/topBanner").css("img_h")
//                        ))
//                        .addColumn(Column.span12().appendChild(
//                                img(DomGlobal.location.getOrigin() + "/api/events/"+ eventId +"/files/topBanner").css("img_h")
//                        ))
        this.root.appendChild(
                Row.create()
                        .addColumn(Column.span1())
                        .addColumn(Column.span5().appendChild(
                                img(DomGlobal.location.getOrigin() + "/api/events/" + eventId + "/files/cover").css("img_h")
                        ))
                        .addColumn(Column.span5()
                                .appendChild(h(3).css("center").textContent("Zapraszamy do obejrzenia transmisji"))
                                .appendChild(ticketCodeInput)
                                .appendChild(submitButton)
                                .appendChild(
                                        h(4).css("center")
                                                .add("Nie wiesz jak się zalogować? ")
//                                                .add(Button.createDefault("Kliknij tutaj.").addClickListener(e -> Window.show("INSTRUKCJA", "").open()))
                                                .add(a().textContent("Kliknij tutaj.").css("underline").on(click, e -> Window.show(buildInstruction()).open()))
                                )
                                .appendChild(ticketsRow)
                                .appendChild(h(1))).element());
        ;

        

        if (!ticketUrls.isEmpty()) {
//            ticketUrls.forEach(buyTicketUrlDto -> {
//            ticketsRow.addColumn(Column.span12().appendChild(
//                                    a(String.valueOf(buyTicketUrlDto), "_blank").textContent("Bilety")
//                                            .css("blackFont w100 btn md waves-effect elevation-1 bg-yellow")
//                            ));
//            });
        }

        ticketCodeInput.addEventListener(ENTER, getFormSubmitHandler());
        root.appendChild(h(1).element());
//        root.appendChild(
//                Paragraph.create("W tych niewesołych czasach mamy dla Was rozrywkę na żywo bez wychodzenia z domu - zapraszamy na STAND-UP ONLINE!\n" +
//                        "Na żywo przed wirtualną publicznością zaprezentują się topowi gracze i graczki polskiego stand-upu, " +
//                        "będą połączenia z niecodziennymi gośćmi, będą skecze, roboty, roasty, prawda i kłamstwa, a do tego oficjalny MVP - Pies Który Gra Na Trąbce (znak tow. zastrzeżony).\n").alignJustify());
//        root.appendChild(Paragraph.create("Zapraszamy więc wszystkich, jak świat długi i szeroki, przed ekrany telefonów i komputerów.  " +
//                "Udostępnimy Wam stream ze specjalnie przygotowanego programu, stworzonego w absolutnie antyseptycznych warunkach. " +
//                "Rozsiądźcie się wygodnie na kanapie, przygotujcie herbatkę w ukochanym kubku i cieszcie się ekskluzywną rozrywką bez wychodzenia z domu.").alignJustify());
//
        initElement(root);
        KeyboardEvents.listenOn(ticketCodeInput).onEnter(getFormSubmitHandler());
    }

    private Node buildInstruction() {
        return div().element().appendChild(
                Row.create()
                        .addColumn(Column.span6().offset(3, 0).appendChild(img(DomGlobal.location.getOrigin() + "/img/instruction1a.png").css("img_h"))).element()
//                        .addColumn(Column.span12().offset(0,0).appendChild(img(DomGlobal.location.getOrigin() + "/img/instruction1a.png").css("img_h")))
        ).appendChild(Row.create()
                .addColumn(Column.span4().appendChild(img(DomGlobal.location.getOrigin() + "/img/instruction1b.png").css("img_h")))
                .addColumn(Column.span4().appendChild(img(DomGlobal.location.getOrigin() + "/img/instruction1c.png").css("img_h")))
                .addColumn(Column.span4().appendChild(img(DomGlobal.location.getOrigin() + "/img/instruction1d.png").css("img_h")))
                .element()
        );
//        return div().appendChild(
//                Row.create()
//                        .addColumn(Column.span6().appendChild(img(DomGlobal.location.getOrigin() + "/img/instruction1a.png").css("img_h")))
//                        .addColumn(Column.span6().appendChild(img(DomGlobal.location.getOrigin() + "/img/instruction1b.png").css("img_h")))
//        ).appendChild(
//                Row.create()
//                        .addColumn(Column.span6().appendChild(img(DomGlobal.location.getOrigin() + "/img/instruction1c.png").css("img_h")))
//                        .addColumn(Column.span6().appendChild(img(DomGlobal.location.getOrigin() + "/img/instruction1d.png").css("img_h")))
//        ).element();
    }

    private EventListener getFormSubmitHandler() {
        return e -> getController().doLogin(ticketCodeInput.getValue());
    }

    @Override
    public void render() {
        this.ticketCodeInput = TextBox.create("KOD BILETU / NUMER BILETU").setFieldStyle(FieldStyle.LINED);
        this.submitButton = Button.createPrimary("Przejdź do streamu")
                .setSize(ButtonSize.LARGE)
                .setBlock(true)
                .style()
                .get()
                .addClickListener(getFormSubmitHandler());
        initElement(root);
    }
}
