package pl.korbeldaniel.demo.client.filter;

import com.github.nalukit.nalu.client.filter.AbstractFilter;
import pl.korbeldaniel.demo.client.Routes;
import pl.korbeldaniel.demo.client.UiContext;

public class StreamingLoginFilter extends AbstractFilter<UiContext> {

    @Override
    public boolean filter(String route, String... parms) {
        if(route.equals(Routes.STREAMING_EVENTS)
        || route.equals(Routes.CONTACT)
        || route.equals(Routes.OFFER)
        || route.contains(Routes.LOGIN_SHELL)
        || route.equals(Routes.FAQ)) {
            return true;
        }

        if(route.equals(Routes.STREAMING)) {
            return checkIsLoggedInForStream();
        }

        if(route.equals(Routes.EVENTS)
        || route.equals("/admin/events/*/edit")) {
            return checkIsLoggedInForAdmin();
        }
//        if(Routes.STREAMING_EVENTS.equals(route)) {
//            return true;
//        }
//        if (!Routes.STREAMING_LOGIN2.equals(route)) {
//            if(Routes.ADMIN_LOGIN.equals(route)) {
//                return true;
//            } else if(this.context.isLoggedInForAdmin()) {
//                return true;
//            } else if(!this.context.isLoggedInForStream()) {
//                return false;
//            }
//        }
        // When false returned then method redirectTo will be called
        return false;
    }

    @Override
    public String redirectTo() {
        return "www";
    }

    private boolean checkIsLoggedInForStream() {
        return this.context.isLoggedInForStream();
    }


    private boolean checkIsLoggedInForAdmin() {
        return this.context.isLoggedInForAdmin();
    }

    @Override
    public String[] parameters() {
        return new String[]{};
    }
}
