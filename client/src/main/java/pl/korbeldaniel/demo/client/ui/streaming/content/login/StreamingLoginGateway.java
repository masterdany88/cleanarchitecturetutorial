package pl.korbeldaniel.demo.client.ui.streaming.content.login;

import com.github.nalukit.nalu.client.Router;
import org.dominokit.domino.rest.shared.request.CanSend;
import org.dominokit.domino.ui.dialogs.MessageDialog;
import org.dominokit.domino.ui.style.Color;
import pl.korbeldaniel.demo.client.Routes;
import pl.korbeldaniel.demo.client.SessionsResourceFactory;
import pl.korbeldaniel.demo.client.UiContext;
import pl.korbeldaniel.demo.client.widget.UiNotifier;
import pl.korbeldaniel.demo.domain.model.StreamingSessionDto;


public class StreamingLoginGateway {

    CanSend createSession(UiContext context, Router router, StreamingSessionDto dto) {
        return SessionsResourceFactory.INSTANCE
                .createSession(dto)
//                .onBeforeSend(uiLoader::start)
                .onSuccess(sessionDto -> {
                    new UiNotifier().showSuccess("Zalogowano kodem: " + sessionDto.getTicketCode().getCode());
                    context.setLoggedInForStream(true);
                    context.setStreamLoginSession(sessionDto);
                    router.route(Routes.STREAMING);
//                    this.component.display(instruments);
//                    this.uiLoader.stop();
                })
                .onFailed(f -> MessageDialog.createMessage(
                        "Błędne logowanie",
                        "Niestety, wprowadzono błędny kod. Spróbój ponownie."
                        )
                        .error()
                        .setModalColor(Color.RED)
                        .setIconColor(Color.GREY, Color.WHITE).open());
//                .onFailed(f -> new UiNotifier().showError("Wprowadzono zły kod"));
    }
    public CanSend getSessionValidity(String id, String code, Runnable sessionExpiredHandler) {
        return SessionsResourceFactory.INSTANCE
                .getSessionValidity(id, code)
//                .onBeforeSend(uiLoader::start)
                .onSuccess(responseDto -> {
                    if("Ok".equals(responseDto.getStatus())) {
//                        new UiNotifier().showSuccess("Sesja ok  " + responseDto.getStatus());
                    } else {
                        new UiNotifier().showError("Twój kod został wykorzystany na innym urządzeniu. Zostaniesz wylogowany");
                        sessionExpiredHandler.run();
                    }
                });
    }
}
