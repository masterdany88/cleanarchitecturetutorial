package pl.korbeldaniel.demo.client.ui.admin.content.events;

import com.github.nalukit.nalu.client.component.IsComponent;
import elemental2.dom.HTMLElement;
import pl.korbeldaniel.demo.domain.model.event.EventDto;

public interface IEventComponent extends IsComponent<IEventComponent.Controller, HTMLElement> {
  void edit(EventDto eventDto, String jwt);

  interface Controller extends IsComponent.Controller {
      void submit(EventDto dto);
  }
}
