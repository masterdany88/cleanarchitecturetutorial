package pl.korbeldaniel.demo.client.ui.admin.popup.error;

import com.github.nalukit.nalu.client.component.AbstractErrorPopUpComponentController;
import com.github.nalukit.nalu.client.component.annotation.ErrorPopUpController;
import java.lang.Override;

import pl.korbeldaniel.demo.client.Routes;
import pl.korbeldaniel.demo.client.UiContext;
@ErrorPopUpController(
    componentInterface = IErrorComponent.class,
    component = ErrorComponent.class
)
public class ErrorController extends AbstractErrorPopUpComponentController<UiContext, IErrorComponent> implements IErrorComponent.Controller {
  public ErrorController() {
  }

  @Override
  public void onBeforeShow() {
    this.component.clear();
  }

  @Override
  public void show() {
    this.component.edit(this.errorEventType, this.route, this.message, this.dataStore);
    this.component.show();
  }

  @Override
  public void doRouteHome() {
    this.router.route(Routes.CONTACT);
  }
}
