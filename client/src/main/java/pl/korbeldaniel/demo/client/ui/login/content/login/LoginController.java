package pl.korbeldaniel.demo.client.ui.login.content.login;

import com.github.nalukit.nalu.client.component.AbstractComponentController;
import com.github.nalukit.nalu.client.component.annotation.Controller;
import elemental2.dom.HTMLElement;
import org.dominokit.domino.ui.dialogs.MessageDialog;
import org.dominokit.domino.ui.notifications.Notification;
import org.dominokit.domino.ui.style.Color;
import pl.korbeldaniel.demo.client.Routes;
import pl.korbeldaniel.demo.client.SessionsResourceFactory;
import pl.korbeldaniel.demo.client.UiContext;
import pl.korbeldaniel.demo.client.widget.UiNotifier;
import pl.korbeldaniel.demo.domain.model.AdminSessionDto;

@Controller(
        route = Routes.ADMIN_LOGIN,
        selector = "content",
        componentInterface = ILoginComponent.class,
        component = LoginComponent.class
)
public class LoginController extends AbstractComponentController<UiContext, ILoginComponent, HTMLElement> implements ILoginComponent.Controller {

    @Override
    public void doLogin(String login, String password) {
        SessionsResourceFactory.INSTANCE.createSession(new AdminSessionDto(login, password))
                .onSuccess(r -> {
                    Notification.createSuccess(r).show();
                    this.context.setJwt("Bearer " +r);
                    this.context.setLoggedInForAdmin(true);
                    new UiNotifier().showSuccess("Zalogowano");
                    router.route(Routes.EVENTS);
                })
                .onFailed(f -> MessageDialog.createMessage(
                        "Błędne logowanie",
                        "Spróbój ponownie."
                )
                        .error()
                        .setModalColor(Color.RED)
                        .setIconColor(Color.GREY, Color.WHITE).open())
                .send();
//        return "supertrudnetymczasowehaslo12345!@#$%ZXCV".equals(login) && "supertrudnetymczasowehaslo12345!@#$%ZXCV".equals(password);
//        return true;
    }
}
