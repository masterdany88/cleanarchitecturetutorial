package pl.korbeldaniel.demo.client.ui.admin.content.screen03;

import com.github.nalukit.nalu.client.component.IsComponent;
import elemental2.dom.HTMLElement;
import pl.korbeldaniel.demo.client.MyModel;
public interface IScreen03Component extends IsComponent<IScreen03Component.Controller, HTMLElement> {
  void edit(MyModel model);

  boolean isDirty();

  boolean isValid();

  void flush(MyModel model);

  interface Controller extends IsComponent.Controller {
  }
}
