package pl.korbeldaniel.demo.client.ui.streaming.content.streaming;

import com.github.nalukit.nalu.client.component.AbstractComponent;
import elemental2.dom.HTMLDivElement;
import elemental2.dom.HTMLElement;
import elemental2.dom.HTMLIFrameElement;
import org.dominokit.domino.ui.button.Button;
import org.dominokit.domino.ui.grid.Column;
import org.dominokit.domino.ui.grid.Row;
import org.dominokit.domino.ui.grid.Row_12;
import org.dominokit.domino.ui.grid.flex.FlexItem;
import org.dominokit.domino.ui.grid.flex.FlexJustifyContent;
import org.dominokit.domino.ui.grid.flex.FlexLayout;
import org.dominokit.domino.ui.style.Color;
import org.dominokit.domino.ui.utils.DominoElement;
import org.jboss.elemento.HtmlContentBuilder;

import static org.jboss.elemento.Elements.*;

public class StreamingComponent extends AbstractComponent<IStreamingComponent.Controller, HTMLElement> implements IStreamingComponent {
    //    private HtmlContentBuilder<HTMLIFrameElement> streamPlaceholder = iframe("https://player.vimeo.com/video/174179940?byline=0&portrait=0");
    private HtmlContentBuilder<HTMLIFrameElement> streamPlaceholder = iframe();
    private Button logoutButton;
    private DominoElement<HTMLDivElement> root = DominoElement.div();
    private DominoElement<HTMLDivElement> iframePlaceholder = DominoElement.div();

    @Override
    public void render() {
        this.root.appendChild(
                Row.create()
                        .addColumn(Column.span12().appendChild(
                                h(2).css("standup").textContent("WYDARZENIE")
                        ))
        );
//        root.appendChild(b);

//        root.setInnerHtml("<iframe src=\"https://player.vimeo.com/video/408787915\" frameborder=\"0\" allow=\"autoplay; fullscreen\" allowfullscreen style=\"position:absolute;top:0;left:0;width:100%;height:100%;\"></iframe>");
//        root.setInnerHtml("aaaa");
//        iframePlaceholder.setInnerHtml("<div style=\"padding:56.25% 0 0 0;position:relative;\"><iframe src=\"https://player.vimeo.com/video/409164313\" frameborder=\"0\" allow=\"autoplay; fullscreen\" allowfullscreen style=\"position:absolute;top:0;left:0;width:100%;height:100%;\"></iframe></div>");
//        iframePlaceholder.setInnerHtml("<div style=\"padding:56.25% 0 0 0;position:relative;\"><iframe src=\"https://player.vimeo.com/video/409254790\" frameborder=\"0\" allow=\"autoplay; fullscreen\" allowfullscreen style=\"position:absolute;top:0;left:0;width:100%;height:100%;\"></iframe></div>");
//        streamPlaceholder.attr("width", "100%");
//        Card card = Card.create("Streaming", "Description text here...")
//                .setBackground(Color.AMBER)
//                //.appendChild(TextNode.of(SAMPLE_CONTENT))
//                .appendChild(streamPlaceholder.that())
//                .addHeaderAction(
//                        Icons.NAVIGATION_ICONS.close(),
//                        (event) -> getController().logout());
//        initElement(card.element());
        logoutButton = Button.create("Wyloguj");
        logoutButton.setBlock(true);
        logoutButton.setBackground(Color.RED_DARKEN_4);
        logoutButton.addClickListener(e -> {
            getController().logout();
        });
        root.appendChild(iframePlaceholder);
//        root.appendChild(FlexLayout.create()
//                .setJustifyContent(FlexJustifyContent.CENTER)
//                .appendChild(FlexItem.create()
//                        .appendChild(iframePlaceholder))
//        );
        root.appendChild(Row.create());
        root.appendChild(
                ol().add(li().textContent("Wydarzenie najlepiej oglądać na przeglądarce Chrome."))
                        .add(li().textContent("Podczas oglądania prosimy o wyłączenie dodatków typu AdBlock itp. i innych programów mogących obciążać urządzenie oraz blokować prawidłową pracę odtwarzacza."))
                        .add(li().textContent("Transmisja się przycina, zawiesza.. - zmień ustawienia jakości (ikonka zębatki w prawym dolnym rogu). Wybór mniejszej jakości poprawia stabilność połączenia. Prędkość i jakość połączenia można sprawdzić tutaj: https://www.speedtest.pl/ - prosimy o uwzględnienie wyników testu podczas ewentualnego składania reklamacji."))
                        .add(li().textContent("Wylogowuje mnie w trakcie oglądania.. Pamiętaj, jeden bilet uprawnia do oglądania transmisji na jednym urządzeniu. Sprawdź, czy nie zalogowałeś się na innym urządzeniu jednocześnie."))
                        );
        root.appendChild(Row.create());
        root.appendChild(Row_12.create()
                .appendChild(Column.span3())
                .appendChild(Column.span6().centerContent().appendChild(logoutButton))
                .appendChild(Column.span3())
        );

        initElement(root.element());
    }

    public void display(String pageContent) {
        iframePlaceholder.setInnerHtml(pageContent);
    }
}
