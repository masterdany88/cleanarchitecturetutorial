package pl.korbeldaniel.demo.client.widget;

import elemental2.dom.EventListener;
import elemental2.dom.Node;
import org.dominokit.domino.ui.button.Button;
import org.dominokit.domino.ui.modals.IsModalDialog;
import org.dominokit.domino.ui.modals.ModalDialog;
import org.dominokit.domino.ui.style.Color;

public class Window {
    public static ModalDialog show(String title, Node content) {
        ModalDialog modal = ModalDialog.create(title).setAutoClose(true);
        modal.setModalColor(Color.BLUE);
//        modal.setModalColor(Color.YELLOW);
        modal.appendChild(content);
        Button closeButton = Button.create("ZAMKNIJ").linkify();
        EventListener closeModalListener = (evt) -> modal.close();
        closeButton.addClickListener(closeModalListener);
        modal.appendFooterChild(closeButton);
        return modal;
    }

    public static ModalDialog show(Node content) {
        ModalDialog modal = ModalDialog.create().setAutoClose(true);
        modal.hideHeader();
//        modal.hideFooter();
        modal.setModalColor(MyColor.STANDUP_YELLOW);
//        modal.setModalColor(Color.YELLOW);
        modal.appendChild(content);
        modal.hideHeader();
        modal.setSize(IsModalDialog.ModalSize.LARGE);
        modal.getBodyElement().css("verticalScrolling");
        Button closeButton = Button.create("ZAMKNIJ").linkify();
        EventListener closeModalListener = (evt) -> modal.close();
        closeButton.addClickListener(closeModalListener);
        modal.appendFooterChild(closeButton);
        return modal;
    }
}
