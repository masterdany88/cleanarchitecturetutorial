@ResourceList({
        EventsResource.class,
        SessionsResource.class,
        UsersResource.class,
        BuyResource.class
})
package pl.korbeldaniel.demo.client;

import org.dominokit.domino.rest.shared.request.service.annotations.ResourceList;
import pl.korbeldaniel.demo.api.resources.BuyResource;
import pl.korbeldaniel.demo.api.resources.EventsResource;
import pl.korbeldaniel.demo.api.resources.UsersResource;
import pl.korbeldaniel.demo.api.resources.SessionsResource;
