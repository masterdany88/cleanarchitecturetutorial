package pl.korbeldaniel.demo.client.ui.streaming.content.offer;

import com.github.nalukit.nalu.client.component.IsComponent;
import elemental2.dom.HTMLElement;

public interface IOfferComponent extends IsComponent<IOfferComponent.Controller, HTMLElement> {

    interface Controller extends IsComponent.Controller {
    }
}
