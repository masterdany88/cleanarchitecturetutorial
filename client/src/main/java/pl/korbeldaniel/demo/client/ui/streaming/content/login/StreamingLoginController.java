package pl.korbeldaniel.demo.client.ui.streaming.content.login;

import com.github.nalukit.nalu.client.component.AbstractComponentController;
import com.github.nalukit.nalu.client.component.annotation.AcceptParameter;
import com.github.nalukit.nalu.client.component.annotation.Controller;
import elemental2.dom.DomGlobal;
import elemental2.dom.HTMLElement;
import elemental2.dom.Notification;
import pl.korbeldaniel.demo.client.EventsResourceFactory;
import pl.korbeldaniel.demo.client.Routes;
import pl.korbeldaniel.demo.client.UiContext;
import pl.korbeldaniel.demo.client.ui.admin.content.events.EventGateway;
import pl.korbeldaniel.demo.client.ui.admin.content.events.EventsGateway;
import pl.korbeldaniel.demo.client.widget.UiNotifier;
import pl.korbeldaniel.demo.domain.model.StreamingSessionDto;
import pl.korbeldaniel.demo.domain.model.ticket.TicketCodeDto;

import java.util.Date;

import static pl.korbeldaniel.demo.client.Routes.ID;

@Controller(
        route = Routes.STREAMING_LOGIN2,
        selector = "content",
        componentInterface = IStreamingLoginComponent.class,
        component = StreamingLoginComponent.class
)
public class StreamingLoginController extends AbstractComponentController<UiContext, IStreamingLoginComponent, HTMLElement> implements IStreamingLoginComponent.Controller {
    private String id;

    @AcceptParameter(ID)
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public void start() {
        EventsResourceFactory.INSTANCE
                .getSummaryById(Long.valueOf(id))
                .onSuccess(r -> {
                    this.component.display(String.valueOf(r.getId()), r.getBuyTicketUrls());
                })
                .onFailed(f -> {
                    new UiNotifier().showError("Wystąpił błąd przy pobieraniu");
                }).send();
    }

    @Override
    public void doLogin(String ticketCode) {
        validateCredentials(ticketCode);
    }

    private void validateCredentials(String ticketCode) {
        new StreamingLoginGateway().createSession(
                context, router,
                new StreamingSessionDto(new TicketCodeDto(id, ticketCode))).send();
    }
}
