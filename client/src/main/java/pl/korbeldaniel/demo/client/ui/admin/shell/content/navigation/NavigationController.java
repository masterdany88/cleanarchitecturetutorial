package pl.korbeldaniel.demo.client.ui.admin.shell.content.navigation;

import com.github.nalukit.nalu.client.component.AbstractComponentController;
import com.github.nalukit.nalu.client.component.annotation.Controller;
import elemental2.dom.HTMLElement;
import java.lang.Override;
import java.lang.String;

import pl.korbeldaniel.demo.client.UiContext;
@Controller(
    route = "/admin/",
    selector = "navigation",
    componentInterface = INavigationComponent.class,
    component = NavigationComponent.class
)
public class NavigationController extends AbstractComponentController<UiContext, INavigationComponent, HTMLElement> implements INavigationComponent.Controller {
  public NavigationController() {
  }

  @Override
  public void doNavigateTo(String target) {
    switch (target) {
      case "events":
      router.route("/admin/events");
      break;
      case "screen02":
      router.route("/admin/screen02");
      break;
      case "screen03":
      router.route("/admin/screen03");
      break;
    }
  }
}
