package pl.korbeldaniel.demo.client.ui.streaming.content.offer;

import com.github.nalukit.nalu.client.component.AbstractComponent;
import elemental2.dom.HTMLDivElement;
import elemental2.dom.HTMLElement;
import org.dominokit.domino.ui.Typography.Paragraph;
import org.dominokit.domino.ui.grid.Column;
import org.dominokit.domino.ui.grid.Row;
import org.dominokit.domino.ui.utils.DominoElement;

import static org.jboss.elemento.Elements.*;

public class OfferComponent extends AbstractComponent<IOfferComponent.Controller, HTMLElement> implements IOfferComponent {
    private DominoElement<HTMLDivElement> root = DominoElement.div();

    @Override
    public void render() {
        root.appendChild(Row.create()
                .addColumn(Column.span12().appendChild(
                        h(2).css("standup").textContent("OFERTA")
                )));
        root.appendChild(Row.create()
                .addColumn(Column.span4().centerContent().appendChild(img("img/offer/1.jpg").css("img_h")))
                .addColumn(Column.span8()
                        .appendChild(Paragraph.create("Korzystając z Międzynarodowych technologii wypracowaliśmy rozwiązanie które jest sprawdzone i pozwoli na przeprowadzenie transmisji w sposób profesjonalny. Opłata zależy od ilości odbiorców przez co oferta jest przejrzysta:"))
                        .appendChild(
                                ul()
                                        .add(li().textContent("Dedykowana strona z własnym brandingiem"))
                                        .add(li().textContent("Możliwość transmisji PPV – wydarzenie tylko dla osób które zakupiły bilet"))
                                        .add(li().textContent("Zabezpieczenia przed kopiowaniem treści i nieuprawnionym dostępem"))
                        )
                        .appendChild(Paragraph.create("Zapewnimy również kompleksową realizację twojego projektu. współpracujemy z ekipą produkcyjną która podoła nawet największym wyzwaniom. Posiadamy sprzęt do" +
                                "streamingu, oświetlenie, nagłośnienie, kamery oraz możliwość przeprowadzenia trasmisji w zależności od potrzeb, dzięki czemu twoje wydarzenie będzie transmitowane w sposób profesjonalny.")
                        )
                )
        );
        root.appendChild(Row.create().addColumn(Column.span12()));
        root.appendChild(Row.create()
                .addColumn(Column.span6()
                        .appendChild(Paragraph.create(
                                "Byliśmy pierwsi na rynku jeżeli chodzi o transmisję wydarzeń komediowych na żywo. obecnie tworzymy kolejną edycję kultowego projektu „stand-up online” oraz uruchamiamy wydarzenia dla kolejnych komików, dlatego oprócz wiedzy technicznej stoi za nami praktyka. Zapraszamy do kontaktu – razem uda nam się przenieść wydarzenia do świata internetu."
                        ).alignRight())
                )
                .addColumn(Column.span6().centerContent().appendChild(img("img/offer/2.jpg").css("img_h")))
        );
        initElement(root.element());
    }

}
