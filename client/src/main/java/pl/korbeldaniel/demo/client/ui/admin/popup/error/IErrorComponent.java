package pl.korbeldaniel.demo.client.ui.admin.popup.error;

import com.github.nalukit.nalu.client.component.IsErrorPopUpComponent;
import com.github.nalukit.nalu.client.event.model.ErrorInfo;
import java.lang.String;
import java.util.Map;
public interface IErrorComponent extends IsErrorPopUpComponent<IErrorComponent.Controller> {
  void clear();

  void edit(ErrorInfo.ErrorType errorEventType, String route, String message,
      Map<String, String> dataStore);

  interface Controller extends IsErrorPopUpComponent.Controller {
    void doRouteHome();
  }
}
