package pl.korbeldaniel.demo.client.ui.admin.content.screen02;

import com.google.gwt.core.client.GWT;
import com.google.gwt.editor.client.Editor;
import de.knightsoftnet.validators.client.editor.BeanValidationEditorDriver;
import elemental2.dom.HTMLDivElement;
import elemental2.dom.HTMLElement;
import org.dominokit.domino.ui.button.Button;
import org.dominokit.domino.ui.forms.*;
import org.dominokit.domino.ui.forms.validations.ValidationResult;
import org.dominokit.domino.ui.icons.Icons;
import org.dominokit.domino.ui.notifications.Notification;
import org.dominokit.domino.ui.utils.DominoElement;
import org.jboss.elemento.IsElement;
import pl.korbeldaniel.demo.domain.model.UserDto;

import java.util.function.Consumer;

import static org.jboss.elemento.Elements.div;

public class UserEditor implements Editor<UserDto>, IsElement<HTMLElement> {

    private UserEditor.Driver driver = GWT.create(UserEditor.Driver.class);
    interface Driver extends BeanValidationEditorDriver<UserDto, UserEditor> {}
    private Consumer<UserDto> onSaveHandler = userDto -> {};
    private FieldsGrouping fieldsGrouping = FieldsGrouping.create();

    private DominoElement<HTMLDivElement> rootElement;
    public LongBox id;
    public TextBox email;
    public TextBox name;
    public IntegerBox age;
    private Button saveButton;

    static UserEditor create(UserDto userDto, Consumer<UserDto> onSaveHandler) {
        UserEditor userEditor = new UserEditor();
        userEditor.onSaveHandler = onSaveHandler;
        userEditor.edit(userDto);
        return userEditor;
    }

    private UserEditor() {
        rootElement = DominoElement.of(div());
        id = LongBox.create("Id")
                .setFieldStyle(FieldStyle.LINED)
                .setMaxLength(50)
                .addLeftAddOn(Icons.ALL.edit())
                .setHelperText("User's id")
                .setReadOnly(true);
        email = TextBox.create("E-mail")
                .setFieldStyle(FieldStyle.LINED)
                .setRequired(true)
                .setAutoValidation(true)
                .groupBy(fieldsGrouping)
                .setHelperText("User's e-mail")
                .setReadOnly(false);
        name = TextBox.create("Name")
                .setFieldStyle(FieldStyle.LINED)
                .setRequired(true)
                .setAutoValidation(true)
                .groupBy(fieldsGrouping)
                .setHelperText("User's name")
                .setReadOnly(false);
        age = IntegerBox.create("Age")
                .setFieldStyle(FieldStyle.LINED)
                .setRequired(true)
                .setAutoValidation(true)
                .groupBy(fieldsGrouping)
                .setHelperText("User's age")
                .setMinValue(0)
                .setMaxValue(150)
                .setReadOnly(false);
        saveButton = Button.create("Update").addClickListener(
                evt -> onSave()
        );

        rootElement.appendChild(id);
        rootElement.appendChild(email);
        rootElement.appendChild(name);
        rootElement.appendChild(age);
        rootElement.appendChild(saveButton);
    }

    @Override
    public HTMLElement element() {
        return rootElement.element();
    }

    void edit(UserDto userDto) {
        driver.initialize(this);
        driver.edit(userDto);
    }

    private void onSave() {
        ValidationResult validationResult = fieldsGrouping.validate();
        if (validationResult.isValid()) {
//            if (nonNull(onSaveHandler)) {
//                onSaveHandler.accept(driver.flush());
//            }
//        } else {
//            Notification.createDanger("Fail to update because " + validationResult.getErrorMessage()).show();
//        }
//    }
//    public void save() {
            UserDto updatedUser = driver.flush();
            if (driver.hasErrors()) {
                Notification.createDanger("Fail to update " + driver.getErrors()).show();
            } else {
                //getController().persistUser(updatedUser);
                onSaveHandler.accept(updatedUser);
            }
        }
    }
}
