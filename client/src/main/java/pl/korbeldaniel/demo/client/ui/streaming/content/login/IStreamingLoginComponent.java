package pl.korbeldaniel.demo.client.ui.streaming.content.login;

import com.github.nalukit.nalu.client.component.IsComponent;
import elemental2.dom.HTMLElement;
import pl.korbeldaniel.demo.domain.model.event.BuyTicketUrlDto;

import java.lang.String;
import java.util.List;

public interface IStreamingLoginComponent extends IsComponent<IStreamingLoginComponent.Controller, HTMLElement> {
    void display(String id, List<BuyTicketUrlDto> ticketsUrl);

    interface Controller extends IsComponent.Controller {
    void doLogin(String ticketCode);
  }
}
