package pl.korbeldaniel.demo.client.ui.admin.shell.content.statusbar;

import com.github.nalukit.nalu.client.component.AbstractComponentController;
import com.github.nalukit.nalu.client.component.annotation.Controller;
import elemental2.dom.HTMLElement;
import java.lang.Override;
import org.gwtproject.event.shared.HandlerRegistration;
import pl.korbeldaniel.demo.client.UiContext;
import pl.korbeldaniel.demo.client.event.StatusChangeEvent;

@Controller(
    route = "/admin/",
    selector = "footer",
    componentInterface = IStatusbarComponent.class,
    component = StatusbarComponent.class
)
public class StatusbarController extends AbstractComponentController<UiContext, IStatusbarComponent, HTMLElement> implements IStatusbarComponent.Controller {
  private HandlerRegistration registration;

  public StatusbarController() {
  }

  @Override
  public void start() {
    this.registration = this.eventBus.addHandler(StatusChangeEvent.TYPE, e -> component.edit(e.getStatus()));
  }

  @Override
  public void stop() {
    this.registration.removeHandler();
  }
}
