package pl.korbeldaniel.demo.client.generics;

import com.github.nalukit.nalu.client.component.IsComponent;
import elemental2.dom.HTMLElement;
import pl.korbeldaniel.demo.client.widget.UiContentLoader;
import pl.korbeldaniel.demo.client.widget.UiNotifier;

public class GenericGateway<C extends IsComponent<? extends IsComponent.Controller, HTMLElement>> {
    protected UiContentLoader uiContentLoader;
    protected UiNotifier uiNotifier;

    public GenericGateway(C component) {
        this.uiNotifier = new UiNotifier();
        this.uiContentLoader = new UiContentLoader(component.asElement());

    }
}
