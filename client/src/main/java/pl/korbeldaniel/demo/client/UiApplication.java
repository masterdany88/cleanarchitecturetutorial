package pl.korbeldaniel.demo.client;

import com.github.nalukit.nalu.client.application.IsApplication;
import com.github.nalukit.nalu.client.application.annotation.Application;
import com.github.nalukit.nalu.client.application.annotation.Debug;
import com.github.nalukit.nalu.client.application.annotation.Filters;
import com.github.nalukit.nalu.plugin.elemental2.client.DefaultElemental2Logger;
import pl.korbeldaniel.demo.client.filter.StreamingLoginFilter;
@Application(
    context = UiContext.class,
    startRoute = Routes.STREAMING_EVENTS,
    loader = UiLoader.class
)
//@Debug(
//    logger = DefaultElemental2Logger.class,
//    logLevel = Debug.LogLevel.DETAILED
//)
@Filters(
    filterClasses = StreamingLoginFilter.class
)
public interface UiApplication extends IsApplication {
}
