package pl.korbeldaniel.demo.client.ui.streaming.content.events;

import com.github.nalukit.nalu.client.component.IsComponent;
import elemental2.dom.HTMLElement;
import pl.korbeldaniel.demo.domain.model.event.EventDto;

import java.util.List;

public interface IEventsComponent extends IsComponent<IEventsComponent.Controller, HTMLElement> {
    void display(List<EventDto> dto);

    interface Controller extends IsComponent.Controller {
        void routeToEventStream(Long id);
        void showBuyTicketPopUp(Long id);
    }
}
