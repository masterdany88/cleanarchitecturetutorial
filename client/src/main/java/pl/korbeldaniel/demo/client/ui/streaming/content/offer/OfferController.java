package pl.korbeldaniel.demo.client.ui.streaming.content.offer;

import com.github.nalukit.nalu.client.component.AbstractComponentController;
import com.github.nalukit.nalu.client.component.annotation.Controller;
import elemental2.dom.HTMLElement;
import pl.korbeldaniel.demo.client.Routes;
import pl.korbeldaniel.demo.client.UiContext;

@Controller(
        route = Routes.OFFER,
        selector = "content",
        componentInterface = IOfferComponent.class,
        component = OfferComponent.class
)
public class OfferController extends AbstractComponentController<UiContext, IOfferComponent, HTMLElement> implements IOfferComponent.Controller {

}
