package pl.korbeldaniel.demo.client.widget;

import elemental2.dom.DomGlobal;
import elemental2.dom.FormData;
import elemental2.dom.XMLHttpRequest;
import org.dominokit.domino.ui.icons.Icons;
import org.dominokit.domino.ui.notifications.Notification;
import org.dominokit.domino.ui.upload.FileUpload;
import org.dominokit.domino.ui.upload.UploadRequestSender;
import org.jboss.elemento.Elements;

import static pl.korbeldaniel.demo.client.RestConfig.AUTHORIZATION_HEADER;

public class UploadFile {
    public static FileUpload create(String fileName, String url, String headerText, String subHeaderText, String jwt) {
        FileUpload fileUpload =
                FileUpload.create()
                        .setIcon(Icons.ALL.touch_app())
                        .setUrl(DomGlobal.location.getOrigin() + "/api/"+ url)
                        .multipleFiles()
                        .accept("text/csv")
                        .appendChild(Elements.h(3).textContent(headerText))
                        .appendChild(
                                Elements.em()
                                        .textContent(subHeaderText))
                        .onAddFile(
                                (fileItem) -> {
                                    fileItem.setFileName(fileName);
                                    fileItem.addBeforeUploadHandler(
                                            (request, f) -> {
                                        request.setRequestHeader(AUTHORIZATION_HEADER, jwt);
                                    });
                                    fileItem.addErrorHandler(
                                            (request) -> {
                                                Notification.createDanger("Wystąpił błąd podczas dodawania pliku " + request.responseText)
                                                        .show();
                                            });
                                    fileItem.addSuccessUploadHandler(
                                            (request) -> {
                                                Notification.createSuccess("Plik został pomyślnie dodany").show();
                                            });
//                                    fileItem.addRemoveHandler(
//                                            (file) -> {
//                                                Notification.createInfo("Plik został usunięty " + file.name).show();
//                                            });
                                });
//        ticketsFilesUploadPlaceholder.appendChild(fileUpload);
        return fileUpload;
    }
}
