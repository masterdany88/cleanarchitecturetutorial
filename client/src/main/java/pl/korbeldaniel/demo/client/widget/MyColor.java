package pl.korbeldaniel.demo.client.widget;

import org.dominokit.domino.ui.style.Color;

public interface MyColor extends Color {

    Color STANDUP_YELLOW = new Color() {
        @Override
        public String getStyle() {
            return "col-yellow-standup";
        }

        @Override
        public String getName() {
            return "YELLOW-STANDUP";
        }

        @Override
        public String getHex() {
            return "#FFD500";
        }

        @Override
        public String getBackground() {
            return "bg-yellow-standup";
        }
    };
}
