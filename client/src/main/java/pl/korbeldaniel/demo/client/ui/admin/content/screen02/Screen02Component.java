package pl.korbeldaniel.demo.client.ui.admin.content.screen02;

import com.github.nalukit.nalu.client.component.AbstractComponent;
import elemental2.dom.HTMLElement;
import org.dominokit.domino.ui.cards.Card;
import pl.korbeldaniel.demo.domain.model.UserDto;

public class Screen02Component extends AbstractComponent<IScreen02Component.Controller, HTMLElement>
        implements IScreen02Component {

    private Card card;

    @Override
    public void edit(UserDto userDto) {
        card.setTitle(userDto.getName() + " editing");
        UserEditor userEditor = UserEditor.create(userDto, updatedUser -> getController().persistUser(updatedUser));
        card.appendChild(userEditor);
    }

    @Override
    public void render() {
        card = Card.create("");
        initElement(card.element());
    }
}
