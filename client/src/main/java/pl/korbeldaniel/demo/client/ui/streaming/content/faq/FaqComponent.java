package pl.korbeldaniel.demo.client.ui.streaming.content.faq;

import com.github.nalukit.nalu.client.component.AbstractComponent;
import elemental2.dom.HTMLDivElement;
import elemental2.dom.HTMLElement;
import org.dominokit.domino.ui.Typography.Paragraph;
import org.dominokit.domino.ui.grid.Column;
import org.dominokit.domino.ui.grid.Row;
import org.dominokit.domino.ui.utils.DominoElement;

import static org.jboss.elemento.Elements.h;

public class FaqComponent extends AbstractComponent<IFaqComponent.Controller, HTMLElement> implements IFaqComponent {
    private DominoElement<HTMLDivElement> root = DominoElement.div();

    @Override
    public void render() {
        root.appendChild(Row.create()
                .addColumn(Column.span12().appendChild(
                        h(2).css("standup").textContent("FAQ")
                )));
        root.appendChild(Row.create()
                .addColumn(Column.span3())
                .addColumn(Column.span6()
                        .appendChild(Paragraph.create("1. Na jakiej przeglądarce najlepiej oglądać stream"))
                        .appendChild(Paragraph.create("Wydarzenie polecamy oglądać na przeglądarce Chrome"))
                        .appendChild(Paragraph.create(""))
                        .appendChild(Paragraph.create("2. Wydarzenie zacina się:"))
                        .appendChild(Paragraph.create("Podczas oglądania prosimy o wyłączenie dodatków typu AdBlock itp. i innych programów mogących obciążać urządzenie oraz blokować prawidłową pracę odtwarzacza." +
                                "Transmisja się przycina, zawiesza.. - zmień ustawienia jakości (ikonka zębatki w prawym dolnym rogu). Wybór mniejszej jakości poprawia stabilność połączenia. "))
                        .appendChild(Paragraph.create(""))
                        .appendChild(Paragraph.create("3. Nie wiem, czy moje łącze internetowe jest wystarczające:"))
                        .appendChild(Paragraph.create("Prędkość i jakość połączenia zależy od bardzo wielu czynników, orientacyjnie prędkość można sprawdzić tutaj: https://www.speedtest.pl/, zalecana prędkość to min. 10 Mb/s."))
                        .appendChild(Paragraph.create(""))
                        .appendChild(Paragraph.create("4. Wylogowuje mnie w trakcie oglądania:"))
                        .appendChild(Paragraph.create("Pamiętaj, jeden bilet uprawnia do oglądania transmisji na jednym urządzeniu. Sprawdź, czy nie zalogowałeś się na innym urządzeniu jednocześnie."))
                ));
        initElement(root.element());
    }

}
