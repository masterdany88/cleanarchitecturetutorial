package pl.korbeldaniel.demo.client.widget.timer;

import com.gargoylesoftware.htmlunit.javascript.host.html.HTMLTimeElement;
import elemental2.dom.DomGlobal;
import org.gwtproject.timer.client.Timer;
import pl.korbeldaniel.demo.client.widget.UiNotifier;

import java.util.Date;

public class MyTimer {
    private static int oneSecond = 1000;

    public static void runEveryXSeconds(int timeInSeconds, TimedExecution exec) {
        new Timer() {
            @Override
            public void run() {
                exec.execute();
            }
        }.scheduleRepeating(oneSecond * timeInSeconds);
    }

    public static void runOnTime(int timeInSeconds, TimedExecution exec) {
        DomGlobal.window.alert("aa");
        Date eventTime = new Date();
        eventTime.setMinutes(40);
        DomGlobal.window.alert(new Date().toString());
        new Timer() {
            @Override
            public void run() {
                new UiNotifier().showError("AAAAAAAA");
                if(new Date().after(eventTime)) {
                    DomGlobal.window.alert("SSSS");
                }
            }
        }.scheduleRepeating(5000);
    }
}
