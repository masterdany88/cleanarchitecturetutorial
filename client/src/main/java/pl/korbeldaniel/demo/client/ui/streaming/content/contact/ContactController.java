package pl.korbeldaniel.demo.client.ui.streaming.content.contact;

import com.github.nalukit.nalu.client.component.AbstractComponentController;
import com.github.nalukit.nalu.client.component.annotation.Controller;
import elemental2.dom.HTMLElement;
import pl.korbeldaniel.demo.client.EventsResourceFactory;
import pl.korbeldaniel.demo.client.Routes;
import pl.korbeldaniel.demo.client.UiContext;
import pl.korbeldaniel.demo.client.ui.streaming.content.events.EventsComponent;
import pl.korbeldaniel.demo.client.ui.streaming.content.events.IEventsComponent;
import pl.korbeldaniel.demo.client.widget.UiNotifier;

@Controller(
        route = Routes.CONTACT,
        selector = "content",
        componentInterface = IContactComponent.class,
        component = ContactComponent.class
)
public class ContactController extends AbstractComponentController<UiContext, IContactComponent, HTMLElement> implements IContactComponent.Controller {

}
