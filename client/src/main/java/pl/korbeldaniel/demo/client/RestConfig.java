package pl.korbeldaniel.demo.client;

import elemental2.dom.DomGlobal;
import org.dominokit.domino.rest.DominoRestConfig;
import org.dominokit.domino.rest.shared.Response;
import org.dominokit.domino.rest.shared.request.FailedResponseBean;
import org.dominokit.domino.rest.shared.request.ResponseInterceptor;
import org.dominokit.domino.rest.shared.request.ServerRequest;
import pl.korbeldaniel.demo.client.widget.UiNotifier;

public class RestConfig {
    public static String AUTHORIZATION_HEADER = "Authorization";
    static void init() {
        DominoRestConfig.initDefaults();
        DominoRestConfig
                .getInstance()
//                .setDefaultServiceRoot("http://127.0.0.1:9000/api/");
                        .setDefaultServiceRoot(DomGlobal.location.getOrigin() + "/api/");
        setupGlobalInterceptors();
        setupDefaultFailedResponseHandler();
    }

    private static void setupGlobalInterceptors() {
        DominoRestConfig.getInstance()
                .addResponseInterceptor(new ResponseInterceptor() {
                    @Override
                    public void interceptOnSuccess(ServerRequest serverRequest, Response response) {
                        //do something with the success response
                    }

                    @Override
                    public void interceptOnFailed(ServerRequest serverRequest, FailedResponseBean failedResponse) {
                        if (failedResponse.getStatusCode() == 401) {
                            serverRequest.skipFailHandler();
                            new UiNotifier().showError("Your session has expired");
                        }
                        //do something with the failed response, maybe forward to login page.
                    }
                });
    }

    private static void setupDefaultFailedResponseHandler() {
        DominoRestConfig.getInstance().setDefaultFailHandler(failedResponse ->
                            new UiNotifier().showError(
                                    "Failed to communicate with server "
                                    + failedResponse.getStatusText()
                                    + " "
                                    + failedResponse.getStatusCode()));
    }
}
