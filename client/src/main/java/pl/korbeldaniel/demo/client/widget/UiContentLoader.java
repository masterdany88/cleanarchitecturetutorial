package pl.korbeldaniel.demo.client.widget;

import elemental2.dom.HTMLElement;
import org.dominokit.domino.ui.cards.Card;
import org.dominokit.domino.ui.loaders.Loader;
import org.dominokit.domino.ui.loaders.LoaderEffect;

public class UiContentLoader {
    private final Loader loader;
    private Card placeholder;
    private final HTMLElement component;

    public UiContentLoader(HTMLElement component) {
        this.component = component;
        this.placeholder = Card.create().setHeight("300px");
        this.loader = Loader.create(placeholder, LoaderEffect.BOUNCE).setLoadingText("Loading ...");
    }

    public void start() {
        this.component.appendChild(placeholder.element());
        loader.start();
    }

    public void stop() {
        this.component.removeChild(placeholder.element());
        loader.stop();
    }
}
