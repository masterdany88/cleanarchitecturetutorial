package pl.korbeldaniel.demo.client.ui.admin.content.events;

import com.github.nalukit.nalu.client.Router;
import org.dominokit.domino.rest.shared.request.CanSend;
import pl.korbeldaniel.demo.client.EventsResourceFactory;
import pl.korbeldaniel.demo.client.Routes;
import pl.korbeldaniel.demo.client.generics.GenericGateway;
import pl.korbeldaniel.demo.client.widget.UiNotifier;
import pl.korbeldaniel.demo.domain.model.event.EventDto;

import static pl.korbeldaniel.demo.client.RestConfig.AUTHORIZATION_HEADER;

public class EventGateway extends GenericGateway<IEventComponent> {
    private final IEventComponent component;

    public EventGateway(IEventComponent component) {
        super(component);
        this.component = component;
    }

    CanSend createNew(Router router, EventDto dto, String jwt) {
        return EventsResourceFactory.INSTANCE
                .createNew(dto)
                .setHeader(AUTHORIZATION_HEADER, jwt)
                .onSuccess(sessionDto -> {
                    new UiNotifier().showSuccess("Utworzone wydarzenie: " + dto.getName());
                    router.route(Routes.EVENTS);
                })
                .onFailed(f -> new UiNotifier().showError("Wystąpił problem przy tworzeniu wydarzenia"));
    }

    CanSend updateById(Router router, EventDto dto, String jwt) {
        return EventsResourceFactory.INSTANCE
                .updateById(dto.getId(), dto)
                .setHeader(AUTHORIZATION_HEADER, jwt)
                .onSuccess(sessionDto -> {
                    new UiNotifier().showSuccess("Zaktualizowano wydarzenie: " + dto.getName());
                    router.route(Routes.EVENTS);
                })
                .onFailed(f -> new UiNotifier().showError("Wystąpił problem przy aktualizacji wydarzenia"));
    }

    CanSend getById(Long id, String jwt) {
        return EventsResourceFactory.INSTANCE
                .getById(id)
                .setHeader(AUTHORIZATION_HEADER, jwt)
                .onBeforeSend(uiContentLoader::start)
                .onSuccess(dto -> {
                    this.component.edit(dto, jwt);
                    this.uiContentLoader.stop();
                })
                .onFailed(f -> {
                    this.uiContentLoader.stop();
                    super.uiNotifier.showError("Wystąpił błąd przy pobieraniu");
                });
    }
}