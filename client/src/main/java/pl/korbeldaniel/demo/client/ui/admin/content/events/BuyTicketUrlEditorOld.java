package pl.korbeldaniel.demo.client.ui.admin.content.events;

import elemental2.dom.HTMLDivElement;
import elemental2.dom.Node;
import org.dominokit.domino.ui.forms.IntegerBox;
import org.dominokit.domino.ui.forms.TextBox;
import org.dominokit.domino.ui.grid.Column;
import org.dominokit.domino.ui.grid.Row;
import org.dominokit.domino.ui.grid.Row_12;
import org.dominokit.domino.ui.utils.DominoElement;
import pl.korbeldaniel.demo.domain.model.event.BuyTicketUrlDto;

import static org.jboss.elemento.Elements.h;

public class BuyTicketUrlEditorOld extends Node {
    private DominoElement<HTMLDivElement> root = DominoElement.div();

    private TextBox idInput;
    private TextBox urlInput;
    private TextBox nameInput;
    private IntegerBox orderInput;

    public BuyTicketUrlEditorOld() {
        Row_12 row1 = Row.create().addColumn(Column.span12().appendChild(h(3).textContent("Linki do biletów")));
        Row_12 row2 = Row.create();
        this.root.appendChild(row1);
        this.root.appendChild(row2);
        this.idInput = new TextBox("Id").hide();
        this.urlInput = new TextBox("URL");
        this.nameInput = new TextBox("Nazwa");
        this.orderInput = new IntegerBox("Kolejność");
        row2.addColumn(Column.span4().appendChild(urlInput));
        row2.addColumn(Column.span4().appendChild(nameInput));
        row2.addColumn(Column.span4().appendChild(orderInput));
    }

    public static BuyTicketUrlEditorOld edit(BuyTicketUrlDto dto) {
        BuyTicketUrlEditorOld editor = new BuyTicketUrlEditorOld();
        editor.idInput.setValue(dto.getId().toString());
        editor.urlInput.setValue(dto.getUrl());
        editor.nameInput.setValue(dto.getName());
        editor.orderInput.setValue(dto.getOrder());
        return editor;
    }

    public BuyTicketUrlDto flush() {
        BuyTicketUrlDto dto = new BuyTicketUrlDto();
        dto.setId(Long.valueOf(idInput.getValue()));
        dto.setName(nameInput.getValue());
        dto.setOrder(orderInput.getValue());
        return dto;
    }

    public BuyTicketUrlEditorOld element() {
        return this.element();
    }
}
