package pl.korbeldaniel.demo.client.ui.streaming.shell;

import com.github.nalukit.nalu.client.component.AbstractShell;
import com.github.nalukit.nalu.client.component.annotation.Shell;
import elemental2.dom.DomGlobal;
import elemental2.dom.Element;
import elemental2.dom.HTMLDivElement;
import org.dominokit.domino.ui.button.Button;
import org.dominokit.domino.ui.grid.Column;
import org.dominokit.domino.ui.grid.Row;
import org.dominokit.domino.ui.grid.Row_12;
import org.dominokit.domino.ui.utils.DominoElement;
import org.dominokit.domino.ui.utils.TextNode;
import org.jboss.elemento.Elements;
import pl.korbeldaniel.demo.client.Routes;
import pl.korbeldaniel.demo.client.UiContext;

import static org.dominokit.domino.ui.utils.DominoElement.div;
import static org.jboss.elemento.Elements.*;
import static pl.korbeldaniel.demo.client.Routes.*;

@Shell(Routes.STREAMING_EVENTS_SHELL)
public class StreamingEventsShell extends AbstractShell<UiContext> {
    private DominoElement<HTMLDivElement> layout = DominoElement.div();

    @Override
    public void attachShell() {
        Elements.body().css("blackBackground");
        layout.setHeight("100%");
        layout.appendChild(buildHeader());
        layout.appendChild(buildContent());
        layout.appendChild(buildFooter());
        DomGlobal.document.body.appendChild(layout.element());
    }

    private Element buildHeader() {
        return Row.create()
                .addColumn(Column.span2())
                .addColumn(Column.span3().appendChild(h(1).textContent(context.getDomain())))
//                .addColumn(Column.span3().appendChild(h(1).textContent("STANDUP-ONLINE.PL")))
//                .addColumn(Column.span8()
//                                .appendChild(
////                                img("img/topPanel.png").css("img"))
//                                        img(DomGlobal.location.getOrigin() + "/api/events/current/files/topBanner").css("img_h"))
//                )
                .addColumn(Column.span5()
                        .appendChild(buildButton("WYDARZENIA", STREAMING_EVENTS))
//                        .appendChild(buildButton("WYKUP DOSTĘP", STREAMING_EVENTS))
                        .appendChild(buildButton("OFERTA", OFFER))
                        .appendChild(buildButton("FAQ", FAQ))
                        .appendChild(buildButton("KONTAKT", CONTACT))
                        .appendChild(Button.create("REGULAMIN").css("standup").addClickListener(evt -> DomGlobal.window.open("file/Regulamin-streamu.pdf")))
//                        .appendChild(a("file/Regulamin_Standup-Online.pdf", "_blank")
//                                .textContent("REGULAMIN")
//                                .css("blackFont btn md waves-effect elevation-1 bg-yellow")
//                        )
                )
                .addColumn(Column.span2())
                .element();
    }

    private Button buildButton(String title, String route) {
        return Button.create(title).css("standup").addClickListener(evt -> router.route(route));
    }

    private Element buildContent() {
        return Row.create()
                .addColumn(Column.span2())
                .addColumn(Column.span8()
                        .appendChild(
                                div().setId("content")
                        ))
                .addColumn(Column.span2())
                .element();
    }

    private Element buildFooter() {
        Row_12 footerLinks = Row.create().css("myFooterLinks")
                .addColumn(Column.span2())
                .addColumn(Column.span8()
                        .appendChild(
                                                TextNode.of("© 2020 by AGENCJA STAND-UP COMEDY BARTŁOMIEJ KORBEL")
                                        ).centerContent(
                        )
                )
                .addColumn(Column.span2());

        return div().css("myFooter").appendChild(footerLinks).element();
    }

    @Override
    public void detachShell() {
        Elements.body().toggle("blackBackground");
        this.layout.remove();
    }

}
