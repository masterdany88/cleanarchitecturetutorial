package pl.korbeldaniel.demo.client.ui.admin.content.events;

import com.github.nalukit.nalu.client.component.AbstractComponentController;
import com.github.nalukit.nalu.client.component.annotation.Controller;
import elemental2.dom.HTMLElement;
import pl.korbeldaniel.demo.client.Routes;
import pl.korbeldaniel.demo.client.UiContext;
import pl.korbeldaniel.demo.domain.model.event.EventDto;

@Controller(
        route = Routes.EVENTS,
        selector = "content",
        componentInterface = IEventsComponent.class,
        component = EventsComponent.class
)
public class EventsController extends AbstractComponentController<UiContext, IEventsComponent, HTMLElement> implements IEventsComponent.Controller {

    @Override
    public void start() {
        new EventsGateway(this.getComponent()).getAllRequest(context.getJwt()).send();
    }

    @Override
    public void edit(String header, EventDto dto) {
        if (dto.getId() == null) {
            router.route(Routes.EVENT, "new");
        } else {
            router.route(Routes.EVENT, dto.getId().toString());
        }
    }
}
