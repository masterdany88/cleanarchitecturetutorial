package pl.korbeldaniel.demo.client.ui.streaming.content.buy;

import com.github.nalukit.nalu.client.component.AbstractPopUpComponent;
import com.github.nalukit.nalu.client.component.IsPopUpComponent;
import elemental2.dom.HTMLElement;
import org.dominokit.domino.ui.Typography.Paragraph;
import org.dominokit.domino.ui.animations.Animation;
import org.dominokit.domino.ui.animations.Transition;
import org.dominokit.domino.ui.button.Button;
import org.dominokit.domino.ui.grid.Column;
import org.dominokit.domino.ui.grid.Row;
import org.dominokit.domino.ui.grid.flex.FlexItem;
import org.dominokit.domino.ui.grid.flex.FlexJustifyContent;
import org.dominokit.domino.ui.grid.flex.FlexLayout;
import org.dominokit.domino.ui.grid.flex.FlexWrap;
import org.dominokit.domino.ui.icons.Icons;
import org.dominokit.domino.ui.infoboxes.InfoBox;
import org.dominokit.domino.ui.modals.ModalDialog;
import org.dominokit.domino.ui.style.Color;
import pl.korbeldaniel.demo.domain.model.payment.PaymentMethodResponse;

import java.util.List;

import static org.jboss.elemento.Elements.h;
import static org.jboss.elemento.Elements.img;

public class BuyTicketComponent
        extends AbstractPopUpComponent<IBuyTicketComponent.Controller>
        implements IBuyTicketComponent {

    private ModalDialog dialog;

    @Override
    public void render() {
        dialog = ModalDialog.create("Zakup biletu").setModalColor(Color.GREY_DARKEN_3).large().setAutoClose(false);
        dialog.setHeight("600px");
        dialog.appendChild(Paragraph.create("Dokonaj zakupu biletu"));
    }

    @Override
    public void show() {
        dialog.open();
    }

    @Override
    public void show(List<PaymentMethodResponse> data) {
        FlexLayout layout = FlexLayout.create()
                .setWrap(FlexWrap.WRAP_TOP_TO_BOTTOM)
                .setJustifyContent(FlexJustifyContent.CENTER);
        dialog.appendChild(layout);
//        data.forEach(p -> dialog.appendChild(img(p.getMobileImgUrl()).attr("width", "80px")));
        data.forEach(p -> {
            if(p.isStatus()) {
                buildPaymentMethodBlock(layout, p);
            }
        });
        show();
    }

    void buildPaymentMethodBlock(FlexLayout layout, PaymentMethodResponse p) {
        FlexItem flexItem = FlexItem.create().setWidth("150px").css("paymentMethodBlock")
                .appendChild(Paragraph.create(p.getName()).alignCenter())
                .appendChild(img(p.getImgUrl()).attr("width", "100px"));
        flexItem.addClickListener(evt -> {
                    Animation.create(flexItem).transition(Transition.BOUNCE_IN).animate();
                    getController().proceedWithSelectedPaymentMethod(p.getId());
                }
        );

        layout.appendChild(
                flexItem

//                FlexItem.create().appendChild(
//                        InfoBox.create(img(p.getImgUrl())
//                                .attr("width","100px")
//                                .attr("min-width","100px")
//                                .attr("max-width","100px")
//                                .attr("height","100px")
//                                .attr("min-height","100px")
//                                .attr("max-height","100px")
//                                .element(), p.getName(), "")
//                                .setIconBackground(Color.GREY_DARKEN_1)
//                                .setBackground(Color.GREY_DARKEN_4)
//                                .setHoverEffect(InfoBox.HoverEffect.ZOOM)
//                                .setWidth("150px")
//                )

        );
    }

    @Override
    public void hide() {
        dialog.close();
    }

    @Override
    public HTMLElement getElement() {
        return dialog.element();
    }
}
