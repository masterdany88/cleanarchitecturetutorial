package pl.korbeldaniel.demo.client.ui.admin.content.events;

import com.github.nalukit.nalu.client.Router;
import org.dominokit.domino.rest.shared.request.CanSend;
import pl.korbeldaniel.demo.client.EventsResourceFactory;
import pl.korbeldaniel.demo.client.Routes;
import pl.korbeldaniel.demo.client.generics.GenericGateway;
import pl.korbeldaniel.demo.client.widget.UiNotifier;
import pl.korbeldaniel.demo.domain.model.event.EventDto;

import java.util.List;

import static pl.korbeldaniel.demo.client.RestConfig.AUTHORIZATION_HEADER;

public class EventsGateway extends GenericGateway<IEventsComponent> {
    private final IEventsComponent component;

    EventsGateway(IEventsComponent component) {
        super(component);
        this.component = component;
    }

    CanSend getAllRequest(String jwt) {
        return EventsResourceFactory.INSTANCE
                .getAll()
                .setHeader(AUTHORIZATION_HEADER, jwt)
                .onBeforeSend(uiContentLoader::start)
                .onSuccess(dto -> {
                    this.component.display(dto);
                    this.uiContentLoader.stop();
                })
                .onFailed(f -> this.uiContentLoader.stop());
    }
}