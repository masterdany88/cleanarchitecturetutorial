package pl.korbeldaniel.demo.client.ui.streaming.content.streaming;

import com.github.nalukit.nalu.client.component.IsComponent;
import elemental2.dom.HTMLElement;

public interface IStreamingComponent extends IsComponent<IStreamingComponent.Controller, HTMLElement> {
    void display(String pageContent);
  interface Controller extends IsComponent.Controller {
      void logout();
      void checkSession();
  }
}
