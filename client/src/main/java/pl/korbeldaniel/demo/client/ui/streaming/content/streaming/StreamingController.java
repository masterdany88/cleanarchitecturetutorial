package pl.korbeldaniel.demo.client.ui.streaming.content.streaming;

import com.github.nalukit.nalu.client.component.AbstractComponentController;
import com.github.nalukit.nalu.client.component.annotation.Controller;
import elemental2.dom.HTMLElement;
import pl.korbeldaniel.demo.client.EventsResourceFactory;
import pl.korbeldaniel.demo.client.Routes;
import pl.korbeldaniel.demo.client.UiContext;
import pl.korbeldaniel.demo.client.ui.streaming.content.login.StreamingLoginGateway;
import pl.korbeldaniel.demo.client.widget.UiNotifier;
import pl.korbeldaniel.demo.client.widget.timer.MyTimer;

@Controller(
        route = Routes.STREAMING,
        selector = "content",
        componentInterface = IStreamingComponent.class,
        component = StreamingComponent.class
)
public class StreamingController extends AbstractComponentController<UiContext, IStreamingComponent, HTMLElement> implements IStreamingComponent.Controller {

    @Override
    public void logout() {
        this.context.setLoggedInForStream(false);
        this.context.setLoggedInForAdmin(false);
        this.context.setStreamLoginSession(null);
        this.router.route(Routes.STREAMING_EVENTS);
    }

    public void checkSession() {
        new StreamingLoginGateway().getSessionValidity(
                String.valueOf(context.getStreamLoginSession().getId()),
                context.getStreamLoginSession().getTicketCode().getCode(),
                () -> {
                    logout();
                }
        ).send();
    }

    @Override
    public void start() {
        String eventId = context.getStreamLoginSession().getTicketCode().getEventId();
        EventsResourceFactory.INSTANCE
                .getByIdAndTicketCode(Long.valueOf(eventId), context.getStreamLoginSession().getTicketCode().getCode())
                .onSuccess(content -> {
                    this.component.display(content.getPageContent());
                })
                .onFailed(f -> {
                    new UiNotifier().showError("Wystąpił błąd przy pobieraniu");
                }).send();

        if(!context.getStreamLoginSession().getEvent().isManyLoginWithSameCodeAllowed()) {
            MyTimer.runEveryXSeconds(55, this::checkSession);
        }
    }
}
