package pl.korbeldaniel.demo.client.ui.admin.content.events;

import com.github.nalukit.nalu.client.component.AbstractComponent;
import elemental2.dom.HTMLDivElement;
import elemental2.dom.HTMLElement;
import org.dominokit.domino.ui.button.Button;
import org.dominokit.domino.ui.cards.Card;
import org.dominokit.domino.ui.datatable.ColumnConfig;
import org.dominokit.domino.ui.datatable.DataTable;
import org.dominokit.domino.ui.datatable.TableConfig;
import org.dominokit.domino.ui.datatable.store.LocalListDataStore;
import org.dominokit.domino.ui.icons.Icons;
import org.dominokit.domino.ui.style.Color;
import org.dominokit.domino.ui.style.Style;
import org.dominokit.domino.ui.utils.TextNode;
import org.gwtproject.i18n.shared.DateTimeFormat;
import org.jboss.elemento.Elements;
import pl.korbeldaniel.demo.domain.model.event.EventDto;

import java.util.List;

public class EventsComponent extends AbstractComponent<IEventsComponent.Controller, HTMLElement> implements IEventsComponent {

    private HTMLDivElement element = Elements.div().element();
    private LocalListDataStore<EventDto> localListDataStore;
    private Card placeholder = Card.create("Wczytuję");

    @Override
    public void display(List<EventDto> dtos) {
        Button createNewButton = Button.createSuccess("Dodaj nowy");
        createNewButton.addClickListener((evt) -> getController().edit("Tworzenie nowego", new EventDto()));

        TableConfig<EventDto> tableConfig = new TableConfig<>();
        tableConfig
                .addColumn(
                        ColumnConfig.<EventDto>create("edit_save", "")
                                .styleCell((element) -> element.style.setProperty("vertical-align", "top"))
                                .setCellRenderer(
                                        (cell) ->
                                                Icons.ALL.pencil_mdi()
                                                        .clickable()
                                                        .setTooltip("Edit")
                                                        .addClickListener((evt) -> getController().edit("Edycja ", cell.getRecord()))
                                                        .element()))
                .addColumn(
                        ColumnConfig.<EventDto>create("id", "#")
                                .textAlign("right")
                                .asHeader()
                                .setCellRenderer(
                                        (cell) -> TextNode.of(cell.getTableRow().getRecord().getId().toString())))
                .addColumn(
                        ColumnConfig.<EventDto>create("active", "Aktywny")
                                .textAlign("center")
                                .setCellRenderer(
                                        (cell) -> {
                                            if (cell.getTableRow().getRecord().isActive()) {
                                                return Style.of(Icons.ALL.check_circle())
                                                        .setColor(Color.GREEN_DARKEN_3.getHex())
                                                        .element();
                                            } else {
                                                return Style.of(Icons.ALL.highlight_off())
                                                        .setColor(Color.RED_DARKEN_3.getHex())
                                                        .element();
                                            }
                                        }))
                .addColumn(
                        ColumnConfig.<EventDto>create("isOnMainPage", "Na stronie głównej")
                                .textAlign("center")
                                .setCellRenderer(
                                        (cell) -> {
                                            if (cell.getTableRow().getRecord().isOnMainPage()) {
                                                return Style.of(Icons.ALL.check_circle())
                                                        .setColor(Color.GREEN_DARKEN_3.getHex())
                                                        .element();
                                            } else {
                                                return Style.of(Icons.ALL.highlight_off())
                                                        .setColor(Color.RED_DARKEN_3.getHex())
                                                        .element();
                                            }
                                        }))
//                 .addColumn(
//                        ColumnConfig.<EventDto>create("isOnMainPage", "Na stronie głównej")
//                                .textAlign("center")
//                                .setCellRenderer(
//                                        (cell) -> {
//                                                SwitchButton checkbox = SwitchButton.create("Na stronie głównej", "Nie", "Tak")
//                                                        .addChangeHandler(
//                                                                (value) -> Notification.createInfo("test " + value).show())
//                                                        .setOffTitle("Nie")
//                                                        .setOnTitle("Tak");
//                                            //checkbox.setValue(cell.getTableRow().getRecord().isOnMainPage());
//                                            return checkbox.element();


//                                                return SwitchButton.create("Na stronie głównej", "Nie", "Tak")
//                                                        .addChangeHandler(
//                                                                (value) -> Notification.createInfo("test " + value).show())
//                                                        .setOffTitle("Nie")
//                                                        .setOnTitle("Tak").element();
//                                            } else {
//                                                return SwitchButton.create("Na stronie głównej", "Nie", "Tak")
//                                                        .addChangeHandler(
//                                                                (value) -> Notification.createInfo("test " + value).show())
//                                                        .setOffTitle("Nie")
//                                                        .setOnTitle("Tak").element();
//                                        }))
                .addColumn(
                        ColumnConfig.<EventDto>create("name", "Nazwa")
                                .setCellRenderer((cell) -> TextNode.of(cell.getTableRow().getRecord().getName())))
                .addColumn(
                        ColumnConfig.<EventDto>create("date", "Data")
                                .setCellRenderer((cell) -> {
                                    String dateTime = "";
                                    if(cell.getTableRow().getRecord().getDate() != null &&
                                            cell.getTableRow().getRecord().getTime() != null) {
                                        DateTimeFormat dateFormat = DateTimeFormat.getFormat("yyyy-MM-dd");
                                        String textDate = dateFormat.format(cell.getTableRow().getRecord().getDate());
                                        DateTimeFormat timeFormat = DateTimeFormat.getFormat("HH:mm:ss");
                                        String textTime = timeFormat.format(cell.getTableRow().getRecord().getTime());
                                        dateTime = textDate + " " + textTime;
                                    }
                                    return TextNode.of(dateTime);
                                }));


        localListDataStore = new LocalListDataStore<>();
        localListDataStore.setData(dtos);

        DataTable<EventDto> table = new DataTable<>(tableConfig, localListDataStore);
        element.removeChild(placeholder.element());
        element.appendChild(
                Card.create(
                        "Wydarzenia",
                        "")
                        .setCollapsible()
                        .appendChild(createNewButton)
                        .appendChild(table)
                        .element());
        table.load();
    }


    @Override
    public void render() {
        initElement(element);
        element.appendChild(placeholder.element());
    }
}
