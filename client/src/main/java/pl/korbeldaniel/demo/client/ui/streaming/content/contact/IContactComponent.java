package pl.korbeldaniel.demo.client.ui.streaming.content.contact;

import com.github.nalukit.nalu.client.component.IsComponent;
import elemental2.dom.HTMLElement;

public interface IContactComponent extends IsComponent<IContactComponent.Controller, HTMLElement> {

    interface Controller extends IsComponent.Controller {
    }
}
