package pl.korbeldaniel.demo.client.ui.streaming.content.faq;

import com.github.nalukit.nalu.client.component.IsComponent;
import elemental2.dom.HTMLElement;

public interface IFaqComponent extends IsComponent<IFaqComponent.Controller, HTMLElement> {

    interface Controller extends IsComponent.Controller {
    }
}
