package pl.korbeldaniel.demo.client.ui.admin.content.events;

import com.github.nalukit.nalu.client.component.IsComponent;
import elemental2.dom.HTMLElement;
import pl.korbeldaniel.demo.domain.model.event.EventDto;

import java.util.List;

public interface IEventsComponent extends IsComponent<IEventsComponent.Controller, HTMLElement> {

    void display(List<EventDto> dto);

    interface Controller extends IsComponent.Controller {
        void edit(String create_user, EventDto userDto);
    }
}
