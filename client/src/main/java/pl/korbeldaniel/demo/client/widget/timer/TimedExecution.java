package pl.korbeldaniel.demo.client.widget.timer;

@FunctionalInterface
public interface TimedExecution {
    void execute();
}
