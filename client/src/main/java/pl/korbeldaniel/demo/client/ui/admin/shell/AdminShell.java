package pl.korbeldaniel.demo.client.ui.admin.shell;

import com.github.nalukit.nalu.client.component.AbstractShell;
import com.github.nalukit.nalu.client.component.annotation.Shell;
import elemental2.dom.CSSProperties;
import java.lang.Override;

import elemental2.dom.Node;
import org.dominokit.domino.ui.button.Button;
import org.dominokit.domino.ui.layout.Layout;
import org.dominokit.domino.ui.style.Color;
import org.dominokit.domino.ui.style.ColorScheme;
import pl.korbeldaniel.demo.client.Routes;
import pl.korbeldaniel.demo.client.UiContext;

import static pl.korbeldaniel.demo.client.Routes.ADMIN_SHELL;

@Shell(ADMIN_SHELL)
public class AdminShell extends AbstractShell<UiContext> {
  private Layout layout;
  private Button logoutButton;

  @Override
  public void attachShell() {
    logoutButton = Button.create("Wyloguj");
    logoutButton.setBackground(Color.RED_DARKEN_4);
    logoutButton.addClickListener(e -> {
      this.context.setLoggedInForAdmin(false);
      this.context.setLoggedInForStream(false);
      this.router.route(Routes.ADMIN_LOGIN);
    });

    layout = Layout.create(context.getDomain() + " - Zarządzanie")
                                  .show(ColorScheme.INDIGO);
    layout.showFooter()
                  .fixFooter()
                  .getFooter()
                  .element().style.minHeight = CSSProperties.MinHeightUnionType.of("42px");
    layout.getFooter().setId("footer");
    layout.getLeftPanel().setId("navigation");
    layout.hideLeftPanel();
    layout.getContentPanel().setId("content");
    layout.getTopBar().appendChild(logoutButton);
  }

  @Override
  public void detachShell() {
    this.layout.remove();
  }
}
