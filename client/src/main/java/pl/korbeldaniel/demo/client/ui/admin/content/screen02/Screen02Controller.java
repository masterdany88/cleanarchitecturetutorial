package pl.korbeldaniel.demo.client.ui.admin.content.screen02;

import com.github.nalukit.nalu.client.component.AbstractComponentController;
import com.github.nalukit.nalu.client.component.annotation.Controller;
import elemental2.dom.HTMLElement;
import pl.korbeldaniel.demo.client.UiContext;
import pl.korbeldaniel.demo.domain.model.UserDto;

@Controller(
        route = "/admin/screen02",
        selector = "content",
        componentInterface = IScreen02Component.class,
        component = Screen02Component.class
)
public class Screen02Controller extends AbstractComponentController<UiContext, IScreen02Component, HTMLElement> implements IScreen02Component.Controller {

    @Override
    public void start() {
//        UsersResourceFactory.INSTANCE.getUser(32L).onSuccess(user -> component.edit(user)).send();
//        eventBus.fireEvent(new StatusChangeEvent("active screen: >>Screen02<<"));
//        UsersResourceFactory.INSTANCE
//                .getUser(44L)
//                .onSuccess(userDto -> {
//                    Notification.createSuccess(userDto.getId().toString()).show();
//                })
//                .onFailed(failedResponse -> {
//                    Notification.createDanger(failedResponse.getBody().toString()).show();
//                }).send();
    }

    @Override
    public void persistUser(UserDto userDto) {
//        UsersResourceFactory.INSTANCE.update(userDto.getId(), userDto)
//                .onSuccess(s -> Notification.createSuccess(userDto.getName() + "Updated").show())
//                .onFailed(s -> Notification.createDanger("Error").show())
//                .send();
    }
}
