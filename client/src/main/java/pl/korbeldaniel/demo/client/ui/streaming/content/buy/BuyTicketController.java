package pl.korbeldaniel.demo.client.ui.streaming.content.buy;

import com.github.nalukit.nalu.client.component.AbstractPopUpComponentController;
import com.github.nalukit.nalu.client.component.IsPopUpComponentCreator;
import com.github.nalukit.nalu.client.component.annotation.PopUpController;
import pl.korbeldaniel.demo.client.BuyResourceFactory;
import pl.korbeldaniel.demo.client.UiContext;
import pl.korbeldaniel.demo.client.widget.UiContentLoader;
import pl.korbeldaniel.demo.domain.model.payment.PaymentMethodsResponse;

@PopUpController(name = "BuyTicketController",
        component = BuyTicketComponent.class,
        componentInterface = IBuyTicketComponent.class
)
public class BuyTicketController
        extends AbstractPopUpComponentController<UiContext, IBuyTicketComponent>
        implements IBuyTicketComponent.Controller, IsPopUpComponentCreator<IBuyTicketComponent> {
    private UiContentLoader uiContentLoader;

    @Override
    public IBuyTicketComponent createPopUpComponent() {
        return new BuyTicketComponent();
    }

    @Override
    public void show() {
        BuyResourceFactory.INSTANCE
                .getPaymentsMethods()
                .onSuccess( response -> handleSuccess(response))
                .send();
        uiContentLoader = new UiContentLoader(component.getElement());
    }

    private void handleSuccess(PaymentMethodsResponse response) {
        component.show(response.getData());

    }

    @Override
    public void proceedWithSelectedPaymentMethod(Integer selectedMethodId) {
//        BuyResourceFactory.INSTANCE
    }
}
