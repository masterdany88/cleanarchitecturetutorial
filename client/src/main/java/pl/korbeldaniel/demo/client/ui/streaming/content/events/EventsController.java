package pl.korbeldaniel.demo.client.ui.streaming.content.events;

import com.github.nalukit.nalu.client.component.AbstractComponentController;
import com.github.nalukit.nalu.client.component.annotation.Controller;
import com.github.nalukit.nalu.client.component.event.ShowPopUpEvent;
import elemental2.dom.DomGlobal;
import elemental2.dom.HTMLElement;
import org.apache.tapestry.wml.Do;
import pl.korbeldaniel.demo.client.EventsResourceFactory;
import pl.korbeldaniel.demo.client.Routes;
import pl.korbeldaniel.demo.client.UiContext;
import pl.korbeldaniel.demo.client.widget.UiNotifier;
import pl.korbeldaniel.demo.domain.model.OrganizerDto;

@Controller(
        route = Routes.STREAMING_EVENTS,
        selector = "content",
        componentInterface = IEventsComponent.class,
        component = EventsComponent.class
)
public class EventsController extends AbstractComponentController<UiContext, IEventsComponent, HTMLElement> implements IEventsComponent.Controller {

    @Override
    public void start() {
        EventsResourceFactory.INSTANCE
                .getSummaryOfAllActiveByOrganizer(
                        OrganizerDto.getByFullDomain(DomGlobal.document.domain)
                )
                .onSuccess(events -> {
                    this.component.display(events);
                })
                .onFailed(f -> {
                    new UiNotifier().showError("Wystąpił błąd przy pobieraniu");
                }).send();
    }

    @Override
    public void routeToEventStream(Long id) {
        router.route(Routes.STREAMING_LOGIN2, String.valueOf(id));
    }

    @Override
    public void showBuyTicketPopUp(Long id) {
        eventBus.fireEvent(ShowPopUpEvent.show("BuyTicketController").using("id", id.toString()));
    }
}
