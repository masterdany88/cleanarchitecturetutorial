package pl.korbeldaniel.demo.client.ui.admin.content.events;

import com.github.nalukit.nalu.client.component.AbstractComponentController;
import com.github.nalukit.nalu.client.component.annotation.AcceptParameter;
import com.github.nalukit.nalu.client.component.annotation.Controller;
import elemental2.dom.HTMLElement;
import pl.korbeldaniel.demo.client.Routes;
import pl.korbeldaniel.demo.client.UiContext;
import pl.korbeldaniel.demo.domain.model.event.EventDto;

import static pl.korbeldaniel.demo.client.Routes.ID;

@Controller(
        route = Routes.EVENT,
        selector = "content",
        componentInterface = IEventComponent.class,
        component = EventComponent.class
)
public class EventController extends AbstractComponentController<UiContext, IEventComponent, HTMLElement> implements IEventComponent.Controller {
    private String id;

    @AcceptParameter(ID)
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public void start() {
        if ("new".equals(id)) {
            getComponent().edit(new EventDto(), context.getJwt());
        } else {
            new EventGateway(getComponent()).getById(Long.valueOf(id),context.getJwt()).send();
        }
    }

    @Override
    public void submit(EventDto dto) {
        if (dto.getId() == null) {
            new EventGateway(getComponent()).createNew(router, dto, context.getJwt()).send();
        } else {
            new EventGateway(getComponent()).updateById(router, dto, context.getJwt()).send();
        }
    }
}
