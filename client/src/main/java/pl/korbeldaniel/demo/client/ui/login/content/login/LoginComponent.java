package pl.korbeldaniel.demo.client.ui.login.content.login;

import com.github.nalukit.nalu.client.component.AbstractComponent;
import elemental2.dom.EventListener;
import elemental2.dom.HTMLElement;
import org.dominokit.domino.ui.button.Button;
import org.dominokit.domino.ui.button.ButtonSize;
import org.dominokit.domino.ui.cards.Card;
import org.dominokit.domino.ui.forms.FieldsGrouping;
import org.dominokit.domino.ui.forms.TextBox;
import org.dominokit.domino.ui.grid.Column;
import org.dominokit.domino.ui.grid.Row;
import org.dominokit.domino.ui.icons.Icons;
import org.dominokit.domino.ui.keyboard.KeyboardEvents;
import org.dominokit.domino.ui.style.Styles;

import static org.dominokit.domino.ui.utils.DominoElement.div;
import static org.jboss.elemento.Elements.img;

public class LoginComponent extends AbstractComponent<ILoginComponent.Controller, HTMLElement> implements ILoginComponent {
    private FieldsGrouping fieldsGrouping;
    private TextBox loginInput;
    private TextBox passwordInput;
    private Button submitButton;

    @Override
    public void render() {
        fieldsGrouping = FieldsGrouping.create();
        this.loginInput = TextBox.create("Login")
                .groupBy(fieldsGrouping)
                .addLeftAddOn(Icons.ALL.label());
        this.passwordInput = TextBox.password("Hasło")
                .groupBy(fieldsGrouping)
                .addLeftAddOn(Icons.ALL.location_on());
        this.submitButton = Button.createPrimary("Login")
                .setSize(ButtonSize.LARGE)
                .style()
                .setMinWidth("120px")
                .get()
                .addClickListener(getFormSubmitHandler());
        initElement(Card.create("Logowanie do systemu zarządzania do Streamingiem Standup")
                .appendChild(div()
                        .css(Styles.align_center)
                        .css(Styles.padding_30)
                        .css("avatar-container")
                        //.add(img("https://image.freepik.com/free-photo/closeup-of-notebook-screen-showing-qr-code_53876-31579.jpg")
                        .appendChild(img("https://standupbilety.pl/img/logo.png")
                                .css(Styles.img_responsive)
                                //.css(Styles.default_shadow)
                                .css("login-avatar")))

                .appendChild(Row.create()
                        .addColumn(Column.span12()
                                .appendChild(this.loginInput)))
                .appendChild(Row.create()
                        .addColumn(Column.span12()
                                .appendChild(this.passwordInput)))
                .appendChild(Row.create()
                        .setGap("10px")
                        .addColumn(Column.span12()
                                .appendChild(submitButton))
                        .style()
                        .setTextAlign("right"))
                .element());
        KeyboardEvents.listenOn(loginInput).onEnter(getFormSubmitHandler());
        KeyboardEvents.listenOn(passwordInput).onEnter(getFormSubmitHandler());
    }

  private EventListener getFormSubmitHandler() {
    return e -> getController().doLogin(this.loginInput.getValue(),
            this.passwordInput.getValue());
  }
}
