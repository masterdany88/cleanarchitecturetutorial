package pl.korbeldaniel.demo.client.ui.admin.content.events;

import com.github.nalukit.nalu.client.component.AbstractComponent;
import elemental2.dom.DomGlobal;
import elemental2.dom.HTMLDivElement;
import elemental2.dom.HTMLElement;
import org.dominokit.domino.ui.button.Button;
import org.dominokit.domino.ui.cards.Card;
import org.dominokit.domino.ui.datepicker.DateBox;
import org.dominokit.domino.ui.forms.*;
import org.dominokit.domino.ui.grid.Column;
import org.dominokit.domino.ui.grid.Row;
import org.dominokit.domino.ui.style.Color;
import org.dominokit.domino.ui.timepicker.TimeBox;
import org.dominokit.domino.ui.utils.DominoElement;
import org.dominokit.domino.ui.utils.TextNode;
import pl.korbeldaniel.demo.client.widget.UploadFile;
import pl.korbeldaniel.demo.domain.model.OrganizerDto;
import pl.korbeldaniel.demo.domain.model.event.BuyTicketUrlDto;
import pl.korbeldaniel.demo.domain.model.event.EventDto;

import java.util.ArrayList;
import java.util.List;

import static org.jboss.elemento.Elements.h;
import static org.jboss.elemento.Elements.img;

public class EventComponent extends AbstractComponent<IEventComponent.Controller, HTMLElement> implements IEventComponent {
    private DominoElement<HTMLDivElement> root = DominoElement.div();
    private EventDto dto;
    private Card card;
    private TextBox nameInput;
    private List<BuyTicketUrlEditor> ticketsUrlInputs = new ArrayList<>();
    private Card ticketsUrlInputsCard;
    private DateBox dateInput;
    private TimeBox timeInput;
    private Select<OrganizerDto> organizer;
    private SwitchButton activeInput;
    private SwitchButton onMainPageInput;
    private SwitchButton manyLoginWithSameCodeAllowed;
    private Button setVideoOnlyLayout;
    private Button setVideoAndChatLayout;
    private TextArea pageContentInput;
    private DominoElement<HTMLDivElement> pageContentPreview = DominoElement.div();
    private Button submitButton;

    @Override
    public void edit(EventDto eventDto, String jwt) {
        dto = eventDto;
        card.setTitle(eventDto.getName());
        nameInput.setValue(eventDto.getName());
//        ticketsUrlInput.setValue(eventDto.getTicketsUrl());
        dateInput.setValue(eventDto.getDate());
        timeInput.setValue(eventDto.getTime());
        organizer.setValue(eventDto.getOrganizer());
        activeInput.setValue(eventDto.isActive());
        onMainPageInput.setValue(eventDto.isOnMainPage());
        manyLoginWithSameCodeAllowed.setValue(eventDto.isManyLoginWithSameCodeAllowed());
        pageContentInput.setValue(eventDto.getPageContent());
        pageContentPreview.setInnerHtml(eventDto.getPageContent());
        toggleUploaderVisibility(jwt);
        buildEditTicketsUrlsForm(eventDto);
    }

    @Override
    public void render() {
        card = Card.create("");
        nameInput = TextBox.create("Nazwa wydarzenia");
//        ticketsUrlInput = TextBox.create("Link do biletów");
        dateInput = DateBox.create("Data wydarzenia").setPattern("yyyy-MM-dd");
        timeInput = TimeBox.create().setLabel("Godzina wydarzenia");
        organizer = Select.create("Organizator");
        List<SelectOption<OrganizerDto>> organizerOptions = new ArrayList<>();
        for (OrganizerDto value : OrganizerDto.values()) {
            String main = "";
            if(value.isMain()) main = " [GŁÓWNY]";
            organizerOptions.add(SelectOption.create(value,
                    value.getDomain() + main));
        }
        organizer.addOptions(organizerOptions);
        organizer.setValue(OrganizerDto.getMain());
        activeInput = SwitchButton.create("Aktywny", "off", "on")
                .setOffTitle("OFF")
                .setOnTitle("ON");
        onMainPageInput = SwitchButton.create("Na stronie głównej", "off", "on")
                .setOffTitle("OFF")
                .setOnTitle("ON");
        manyLoginWithSameCodeAllowed = SwitchButton.create("Pozwalaj na użycie kodu wiele razy", "off", "on")
                .setOffTitle("OFF")
                .setOnTitle("ON");

        setVideoOnlyLayout = Button.create("Wstaw kod tylko dla wideo");
        setVideoOnlyLayout.addClickListener(evt -> pageContentInput.setValue(
                "<div style=\"display: flex; justify-content: center;\">"+
                "   <!-- PUT HERE VIDEO IFRAME --> \n" +
                "   <iframe src=\"https://player.vimeo.com/video/656975799?h=f521ca1edf\" width=\"640\" height=\"500\" frameborder=\"0\" allow=\"autoplay; fullscreen\" allowfullscreen></iframe>" +
                "</div>"
        ));
        setVideoAndChatLayout = Button.create("Wstaw kod dla wideo i chatu");
        setVideoAndChatLayout.addClickListener(evt -> pageContentInput.setValue(
                "<div class=\"grid-row row-12\">\n" +
                "    <div class=\"grid-col span-xl-9 span-l-9 span-m-9 span-s-full span-xs-full\" >\n" +
                "       <!-- PUT HERE VIDEO IFRAME --> \n" +
                "       <iframe src=\"https://player.vimeo.com/video/656975799?h=f521ca1edf\" width=\"640\" height=\"500\" frameborder=\"0\" allow=\"autoplay; fullscreen\" allowfullscreen></iframe>" +
                "    </div>\n" +
                "    <div class=\"grid-col span-xl-3 span-l-3 span-m-3 span-s-full span-xs-full align-center\" >\n" +
                "       <!-- PUT HERE CHAT IFRAME --> \n" +
                "       <iframe src=\"https://vimeo.com/live-chat/656975799/f521ca1edf\" width=\"200\" height=\"420\" frameborder=\"0\" ></iframe>\n" +
                "    </div>\n" +
                "</div>"
        ));

        pageContentInput = TextArea.create("Kod HTML z serwisów streamingowych");
        pageContentInput.addChangeHandler(e -> pageContentPreview.setInnerHtml(pageContentInput.getValue()));
        submitButton = Button.createSuccess("Zapisz");
        submitButton.addClickListener(e -> getController().submit(collectDtoData()));
        root.appendChild(card.element());
        card.appendChild(nameInput);
        card.appendChild(dateInput);
        card.appendChild(timeInput);
        card.appendChild(organizer);
        card.appendChild(activeInput);
        card.appendChild(onMainPageInput);
        card.appendChild(manyLoginWithSameCodeAllowed);
        card.appendChild(Row.create()
                .addColumn(Column.span6().appendChild(setVideoOnlyLayout))
                .addColumn(Column.span6().appendChild(setVideoAndChatLayout))
        );
        card.appendChild(pageContentInput);
        card.appendChild(
                Card.create("Podgląd dla kodu HTML serwisów streamingowych", "Zaktualizuje się po edycji kodu HTML")
                        .setHeaderBackground(Color.LIGHT_BLUE_LIGHTEN_4)
                        .setBodyBackground(Color.LIGHT_BLUE_LIGHTEN_5)
                        .appendChild(pageContentPreview));
        card.appendChild(submitButton);
        initElement(root.element());
    }

    private EventDto collectDtoData() {
        dto.setName(nameInput.getValue());
//        dto.setTicketsUrl(ticketsUrlInput.getValue());
        ticketsUrlInputs.forEach(editor -> dto.getBuyTicketUrls().add(editor.save()));
        dto.setPageContent(pageContentInput.getValue());
        dto.setDate(dateInput.getValue());
        dto.setTime(timeInput.getValue());
        dto.setActive(activeInput.getValue());
        dto.setOnMainPage(onMainPageInput.getValue());
        dto.setManyLoginWithSameCodeAllowed(manyLoginWithSameCodeAllowed.getValue());
        dto.setOrganizer(organizer.getValue());
        return dto;
    }

    private void toggleUploaderVisibility(String jwt) {
        if (dto.getId() != null) {
            root.appendChild(
                    Card.create("Pliki csv z biletami").appendChild(
                            Row.create()
                                    .addColumn(Column.span6()
                                            .appendChild(
                                                    UploadFile.create("tickets.png", "events/" + dto.getId() + "/files/tickets",
                                                            "Przenieś tu pliki csv biletów, lub kliknij aby wybrać",
                                                            "Wybrane pliki, z kodami zostaną dodanę do kodów wydarzenia. " +
                                                                    "Jeżeli kod był wcześniej wgrany to zostanie zignorowany",
                                                            jwt
                                                    )
                                            ))
                                    .addColumn(Column.span6().appendChild(
                                            TextNode.of("TODO: tickets summary")
                                    ))
                    ));
            root.appendChild(
                    Card.create("Plik z grafiką nagłówka").appendChild(
                            Row.create()
                                    .addColumn(Column.span6()
                                            .appendChild(
                                                    UploadFile.create("topBanner.png", "events/" + dto.getId() + "/files/topBanner",
                                                            "Przenieś tu plik png nagłówka, lub kliknij aby wybrać",
                                                            "Jeżeli obrazek był wcześniej wgrany to zostanie nadpisany",
                                                            jwt
                                                    )
                                            ))
                                    .addColumn(Column.span6()
                                            .appendChild(
                                                    img(DomGlobal.location.getOrigin() + "/api/events/" + dto.getId() + "/files/topBanner").css("img_h")
                                            ))
                    ));
            root.appendChild(
                    Card.create("Plik z okładką wydarzenia").appendChild(
                            Row.create()
                                    .addColumn(Column.span6()
                                            .appendChild(
                                                    UploadFile.create("cover.png", "events/" + dto.getId() + "/files/cover",
                                                            "Przenieś tu plik okładki, lub kliknij aby wybrać",
                                                            "Jeżeli obrazek był wcześniej wgrany to zostanie nadpisany",
                                                            jwt
                                                    )
                                            ))
                                    .addColumn(Column.span6()
                                            .appendChild(
                                                    img(DomGlobal.location.getOrigin() + "/api/events/" + dto.getId() + "/files/cover").css("img_v")
                                            ))));
        }


    }

    private void buildEditTicketsUrlsForm(EventDto eventDto) {
        ticketsUrlInputsCard = Card.create("Linki do biletów");
        eventDto.getBuyTicketUrls().forEach(dto -> {
//            BuyTicketUrlEditorOld editor = BuyTicketUrlEditorOld.edit(dto);
            BuyTicketUrlEditor editor = BuyTicketUrlEditor.create(dto);
            ticketsUrlInputs.add(editor);
            ticketsUrlInputsCard.appendChild(editor);
        });
        BuyTicketUrlEditor editor = BuyTicketUrlEditor.create(new BuyTicketUrlDto());
        ticketsUrlInputs.add(editor);
        ticketsUrlInputsCard.appendChild(editor);
        root.appendChild(ticketsUrlInputsCard);
    }

}
