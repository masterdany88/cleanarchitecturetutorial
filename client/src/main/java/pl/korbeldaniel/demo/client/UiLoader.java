package pl.korbeldaniel.demo.client;

import com.github.nalukit.nalu.client.application.AbstractApplicationLoader;
import com.github.nalukit.nalu.client.application.IsApplicationLoader;
import elemental2.dom.DomGlobal;
import org.apache.tapestry.wml.Do;

public class UiLoader extends AbstractApplicationLoader<UiContext> {
    @Override
    public void load(IsApplicationLoader.FinishLoadCommand finishLoadCommand) {
        // enter your code here ...
        this.context.setDomain(
                DomGlobal.window.location.getHostname()
                        .replace("www.", "")
                        .toUpperCase()
        );
        DomGlobal.document.title = this.context.getDomain();
        finishLoadCommand.finishLoading();
    }
}