package pl.korbeldaniel.demo.client.ui.login.shell;

import com.github.nalukit.nalu.client.component.AbstractShell;
import com.github.nalukit.nalu.client.component.annotation.Shell;
import elemental2.dom.CSSProperties;
import java.lang.Override;
import org.dominokit.domino.ui.layout.Layout;
import org.dominokit.domino.ui.style.ColorScheme;
import pl.korbeldaniel.demo.client.UiContext;

import static pl.korbeldaniel.demo.client.Routes.LOGIN_SHELL;

@Shell(LOGIN_SHELL)
public class LoginShell extends AbstractShell<UiContext> {
  private Layout layout;

  @Override
  public void attachShell() {
    layout = Layout.create(context.getDomain() + " - Zarządzanie")
                                  .show(ColorScheme.RED);
    layout.showFooter()
                  .fixFooter()
                  .getFooter()
                  .element().style.minHeight = CSSProperties.MinHeightUnionType.of("42px");
    layout.getFooter().setId("footer");
    layout.disableLeftPanel();
    layout.getContentPanel().setId("content");
  }

  @Override
  public void detachShell() {
    this.layout.remove();
  }
}
