package pl.korbeldaniel.demo.client.ui.admin.shell.content.navigation;

import com.github.nalukit.nalu.client.component.AbstractComponent;
import elemental2.dom.HTMLElement;
import java.lang.Override;
import org.dominokit.domino.ui.icons.Icons;
import org.dominokit.domino.ui.tree.Tree;
import org.dominokit.domino.ui.tree.TreeItem;
public class NavigationComponent extends AbstractComponent<INavigationComponent.Controller, HTMLElement> implements INavigationComponent {
  private TreeItem EventsItem;

  public NavigationComponent() {
    super();
  }

  @Override
  public void render() {
    this.EventsItem = TreeItem.create("Events", Icons.ALL.list())
                    .addClickListener(e -> getController().doNavigateTo("events"));
    Tree tree = Tree.create("Navigation");
    tree.appendChild(EventsItem);
    initElement(tree.element());
  }
}
