package pl.korbeldaniel.demo.client;

public class Routes {
    private static final String I = "/";
    private static final String PARAM = I + ":";
    private static final String LIST = I + "list";
    private static final String EDIT = I + "edit";
    private static final String LOGIN = "login";

    public static final String ID = "id";
    public static final String ADMIN_SHELL = "admin";
    public static final String LOGIN_SHELL = LOGIN;
    public static final String STREAMING_LOGIN_SHELL = "streaming";
    public static final String STREAMING_EVENTS_SHELL = "events";

    public static final String ADMIN = I + ADMIN_SHELL;
    public static final String ADMIN_LOGIN = I + LOGIN_SHELL + I + LOGIN;
//    public static final String STREAMING_LOGIN = I + STREAMING_LOGIN_SHELL + I + LOGIN;
    public static final String STREAMING_EVENTS = I + STREAMING_EVENTS_SHELL  + LIST;
    public static final String CONTACT = I + STREAMING_EVENTS_SHELL  + I + "contact";
    public static final String FAQ = I + STREAMING_EVENTS_SHELL  + I + "faq";
    public static final String OFFER = I + STREAMING_EVENTS_SHELL  + I + "oferta";
    public static final String STREAMING = I + STREAMING_EVENTS_SHELL + I + "stream";
    public static final String EVENTS = ADMIN + I + "events";
    public static final String EVENT = EVENTS + PARAM + ID + EDIT;
    public static final String STREAMING_LOGIN2 = I + STREAMING_EVENTS_SHELL + PARAM + ID + I + LOGIN;

    public static final String SIGN_OUT = ADMIN + I + "signOut";
}
