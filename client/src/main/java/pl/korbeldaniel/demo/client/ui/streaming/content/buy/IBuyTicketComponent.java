package pl.korbeldaniel.demo.client.ui.streaming.content.buy;

import com.github.nalukit.nalu.client.component.IsPopUpComponent;
import elemental2.dom.HTMLElement;
import pl.korbeldaniel.demo.domain.model.payment.PaymentMethodResponse;

import java.util.List;

public interface IBuyTicketComponent extends IsPopUpComponent<IBuyTicketComponent.Controller> {
    HTMLElement getElement();
    void show(List<PaymentMethodResponse> data);

    interface Controller extends IsPopUpComponent.Controller {
        void proceedWithSelectedPaymentMethod(Integer selectedMethodId);
    }
}
