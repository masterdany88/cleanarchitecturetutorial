package pl.korbeldaniel.demo.client.ui.admin.content.screen02;

import com.github.nalukit.nalu.client.component.IsComponent;
import elemental2.dom.HTMLElement;
import pl.korbeldaniel.demo.domain.model.UserDto;

public interface IScreen02Component extends IsComponent<IScreen02Component.Controller, HTMLElement> {
  void edit(UserDto model);

  interface Controller extends IsComponent.Controller {
    void persistUser(UserDto userDto);
  }
}
