package pl.korbeldaniel.demo.client;

import com.github.nalukit.nalu.client.context.IsContext;
import pl.korbeldaniel.demo.domain.model.StreamingSessionDto;

public class UiContext implements IsContext {
    private boolean loggedInForStream;
    private boolean loggedInForAdmin;
    private String jwt;
    private StreamingSessionDto streamLoginSession;
    private String domain;

    public boolean isLoggedInForStream() {
        return loggedInForStream;
    }

    public void setLoggedInForStream(boolean loggedInForStream) {
        this.loggedInForStream = loggedInForStream;
    }

    public boolean isLoggedInForAdmin() {
        return loggedInForAdmin;
    }

    public void setLoggedInForAdmin(boolean loggedInForAdmin) {
        this.loggedInForAdmin = loggedInForAdmin;
    }

    public StreamingSessionDto getStreamLoginSession() {
        return streamLoginSession;
    }

    public void setStreamLoginSession(StreamingSessionDto streamLoginSession) {
        this.streamLoginSession = streamLoginSession;
    }

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getDomain() {
        return domain;
    }
}
