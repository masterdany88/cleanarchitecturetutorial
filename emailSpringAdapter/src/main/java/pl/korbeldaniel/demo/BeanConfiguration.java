package pl.korbeldaniel.demo;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("pl.korbeldaniel.demo.email")
@EntityScan("pl.korbeldaniel.demo.email")
public class BeanConfiguration {
}
