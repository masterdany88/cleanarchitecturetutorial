package pl.korbeldaniel.demo.email;

import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import pl.korbeldaniel.demo.usecase.port.email.EmailServicePort;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Component
public class EmailServiceAdapter implements EmailServicePort {
    private final JavaMailSender emailSender;
    private final TemplateEngine templateEngine;

    public EmailServiceAdapter(JavaMailSender emailSender, TemplateEngine templateEngine) {
        this.emailSender = emailSender;
        this.templateEngine = templateEngine;
    }

    @Override
    public void sendTicketPurchasedConfirmationEmail(
            String to, String subject, String eventName, String accessCode) {
        Context context = new Context();
        context.setVariable("subject", subject);
        context.setVariable("eventName", eventName);
        context.setVariable("accessCode", accessCode);

        MimeMessage mail = emailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(mail, true);
            helper.setFrom("Standup-online <informacja@standup-online.pl>");
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(templateEngine.process("PurchaseTicketConfirmationMail.html", context), true);

        } catch (MessagingException e) {
            e.printStackTrace();
        }
        try {
            emailSender.send(mail);
        } catch (MailException e) {
            e.printStackTrace();
        }
    }
}
