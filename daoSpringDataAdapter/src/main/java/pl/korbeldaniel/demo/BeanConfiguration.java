package pl.korbeldaniel.demo;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EntityScan("pl.korbeldaniel.demo.repository")
@ComponentScan("pl.korbeldaniel.demo.repository")
@EnableJpaRepositories("pl.korbeldaniel.demo.repository")
public class BeanConfiguration {
}
