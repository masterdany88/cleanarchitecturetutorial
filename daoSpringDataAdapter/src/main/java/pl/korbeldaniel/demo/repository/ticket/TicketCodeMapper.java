package pl.korbeldaniel.demo.repository.ticket;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.korbeldaniel.demo.domain.model.ticket.TicketCodeDto;

import java.util.List;

@Mapper
public interface TicketCodeMapper {
    TicketCodeDto toDto(TicketCodeEntity entity);
    TicketCodeEntity toEntity(TicketCodeDto dto);
    List<TicketCodeDto> toDto(List<TicketCodeEntity> entities);
    List<TicketCodeEntity> toEntity(List<TicketCodeDto> dtos);
    TicketCodeMapper INSTANCE = Mappers.getMapper( TicketCodeMapper.class );
}
