package pl.korbeldaniel.demo.repository.event;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import pl.korbeldaniel.demo.domain.model.OrganizerDto;
import pl.korbeldaniel.demo.domain.model.event.EventDto;
import pl.korbeldaniel.demo.usecase.port.dao.EventDao;

import javax.persistence.EntityNotFoundException;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@Transactional
public class EventDaoImpl implements EventDao {
    private final EventRepository repository;
    private final EventMapper mapper;

    public EventDaoImpl(EventRepository repository) {
        this.repository = repository;
        this.mapper = EventMapper.INSTANCE;
    }

    @Override
    public Optional<Long> findCurrentEventId() {
        return repository.findIdByOnMainPageIsTrue();
    }

    @Override
    public Optional<Long> findCurrentEventIdByWwwDomain(String domain) {
        return repository.findIdByOnMainPageAndOrganizer_DomainContains(domain);
    }

    @Override
    public Optional<EventDto> findById(Long id) {
        Optional<EventEntity> fromDb = repository.findById(id);
        return Optional.ofNullable(mapper.toDto(fromDb.get()));
    }

    @Override
    public EventDto save(EventDto dto) {
        EventEntity fromDb = repository.save(mapper.toEntity(dto));
        return mapper.toDto(fromDb);
    }

    @Override
    public void update(EventDto dto) {
        EventEntity eventEntity = mapper.toEntity(dto);
        Optional<EventEntity> byId = repository.findById(dto.getId());
        if (byId.isPresent()) {
            eventEntity.setTopBannerImage(byId.get().getTopBannerImage());
            eventEntity.setCoverImage(byId.get().getCoverImage());
        }
        repository.save(eventEntity);
    }

    @Override
    public List<EventDto> findAll() {
        return repository.findAll().stream()
                .map(mapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<EventDto> findSummaryOfAllActive() {
        return mapAndSort(repository.findAllByActiveTrue());
    }


    @Override
    public List<EventDto> findSummaryOfAllActiveByOrganizer(OrganizerDto organizer) {
        return mapAndSort(repository.findAllByActiveTrueAndOrganizerEquals(organizer));
    }

    private List<EventDto> mapAndSort(List<EventEntity> events) {
        return events.stream()
                .map(e -> {
                    EventDto dto = mapper.toDto(e);
                    dto.setPageContent("");
                    return dto;
                })
                .sorted((t0, t1) -> {
                    boolean isPast1;
                    boolean isPast2;
                    if (t0.getDate() == null) {
                        isPast1 = false;
                    } else {
                        isPast1 = t0.getDate().before(new Date());
                    }
                    if (t1.getDate() == null) {
                        isPast2 = false;
                    } else {
                        isPast2 = t1.getDate().before(new Date());
                    }

                    if (isPast1 != isPast2)
                        return isPast1 ? 1 : -1;

                    if(t0.getDate() == null && t1.getDate() == null) {
                        return 1;
                    } else if(t0.getDate() == null) {
                        return isPast1 ? t1.getDate().compareTo(new Date()) : new Date().compareTo(t1.getDate());
                    } else if(t1.getDate() == null) {
                        return isPast1 ? new Date().compareTo(t0.getDate()) : t0.getDate().compareTo(new Date());
                    } else {
                        return isPast1 ? t1.getDate().compareTo(t0.getDate()) : t0.getDate().compareTo(t1.getDate());
                    }

                })
                .collect(Collectors.toList());
    }

    public EventDto findSummaryById(Long id) {
        Optional<EventEntity> byId = repository.findById(id);
        if (byId.isPresent()) {
            EventDto dto = EventMapper.INSTANCE.toDto(byId.get());
            dto.setPageContent("");
            return dto;
        } else {
            throw new EntityNotFoundException();
        }
    }

    @Override
    public void updateTopBannerById(byte[] image, Long id) {
        Optional<EventEntity> byId = repository.findById(id);
        if (byId.isPresent()) {
            byId.get().setTopBannerImage(image);
            repository.save(byId.get());
        } else {
            throw new EntityNotFoundException();
        }
    }

    @Override
    public void updateCoverById(byte[] image, Long id) {
        Optional<EventEntity> byId = repository.findById(id);
        if (byId.isPresent()) {
            byId.get().setCoverImage(image);
            repository.save(byId.get());
        } else {
            throw new EntityNotFoundException();
        }
    }

    @Override
    public byte[] findTopBannerById(Long id) {
        return repository.findById(id).get().getTopBannerImage();
    }

    @Override
    public byte[] findCoverById(Long id) {
        return repository.findById(id).get().getCoverImage();
    }
}
