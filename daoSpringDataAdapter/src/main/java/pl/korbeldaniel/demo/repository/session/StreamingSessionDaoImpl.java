package pl.korbeldaniel.demo.repository.session;

import org.springframework.stereotype.Component;
import pl.korbeldaniel.demo.domain.model.StreamingSessionDto;
import pl.korbeldaniel.demo.repository.ticket.TicketCodeEntity;
import pl.korbeldaniel.demo.repository.ticket.TicketCodeRepository;
import pl.korbeldaniel.demo.usecase.port.dao.StreamingSessionDao;

import javax.transaction.Transactional;
import java.util.Optional;

@Component
public class StreamingSessionDaoImpl implements StreamingSessionDao {
    private StreamingSessionRepository streamingSessionRepository;
    private TicketCodeRepository ticketCodeRepository;
    private StreamingSessionMapper mapper;

    public StreamingSessionDaoImpl(StreamingSessionRepository streamingSessionRepository, TicketCodeRepository ticketCodeRepository) {
        this.streamingSessionRepository = streamingSessionRepository;
        this.ticketCodeRepository = ticketCodeRepository;
        this.mapper = StreamingSessionMapper.INSTANCE;
    }

    @Override
    public StreamingSessionDto create(StreamingSessionDto dto) {
        Optional<TicketCodeEntity> ticketCodeEntity = ticketCodeRepository.findById(String.valueOf(dto.getTicketCode().getCode()));
        StreamingSessionEntity entityToSave = mapper.toEntity(dto);
        entityToSave.setTicketCode(ticketCodeEntity.get());
        StreamingSessionEntity fromDb = streamingSessionRepository.save(entityToSave);
        dto = mapper.toDto(fromDb);
        dto.getTicketCode().setEventId(ticketCodeEntity.get().getEvent().getId().toString());
        return dto;
    }

    @Override
    @Transactional
    public StreamingSessionDto findByIdAndTicketCode_code(Long id, String ticketCode) {
        Optional<StreamingSessionEntity> mayBeEntity = streamingSessionRepository.findByIdAndTicketCode_code(id, ticketCode);
        if (mayBeEntity.isPresent()) {
            return mapper.toDto(mayBeEntity.get());
        } else {
            return null;
        }
    }

    @Override
    @Transactional
    public void deleteByTicketCode(String ticketCode) {
        streamingSessionRepository.deleteByTicketCode_code(ticketCode);
    }
}
