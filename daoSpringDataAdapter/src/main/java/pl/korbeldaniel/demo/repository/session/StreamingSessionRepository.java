package pl.korbeldaniel.demo.repository.session;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.korbeldaniel.demo.repository.ticket.TicketCodeEntity;

import java.util.Optional;

@Repository
public interface StreamingSessionRepository extends JpaRepository<StreamingSessionEntity, String> {
    void deleteByTicketCode_code(String ticketCode_code);
    Optional<StreamingSessionEntity> findByIdAndTicketCode_code(Long id, String ticketCode_code);
}
