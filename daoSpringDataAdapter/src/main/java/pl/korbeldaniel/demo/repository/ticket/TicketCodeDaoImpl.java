package pl.korbeldaniel.demo.repository.ticket;

import org.springframework.stereotype.Component;
import pl.korbeldaniel.demo.domain.model.ticket.TicketCodeDto;
import pl.korbeldaniel.demo.repository.event.EventEntity;
import pl.korbeldaniel.demo.repository.event.EventRepository;
import pl.korbeldaniel.demo.usecase.port.dao.TicketCodeDao;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class TicketCodeDaoImpl implements TicketCodeDao {
    private TicketCodeRepository ticketCodeRepository;
    private EventRepository eventRepository;
    private TicketCodeMapper mapper;

    public TicketCodeDaoImpl(TicketCodeRepository ticketCodeRepository, EventRepository eventRepository) {
        this.ticketCodeRepository = ticketCodeRepository;
        this.eventRepository = eventRepository;
        this.mapper = TicketCodeMapper.INSTANCE;
    }

    @Override
    public List<TicketCodeDto> saveAll(List<TicketCodeDto> dtos, Long eventId) {
        EventEntity eventEntity  = eventRepository.findById(eventId).get();
        List<TicketCodeEntity> toSave = new ArrayList<>();
        for(TicketCodeDto dto: dtos) {
            toSave.add(new TicketCodeEntity(dto.getCode(), eventEntity));
        }
        List<TicketCodeEntity> fromDb = ticketCodeRepository.saveAll(toSave);
        return mapper.toDto(fromDb);
    }

    @Override
    @Transactional
    public Optional<TicketCodeDto> findByCodeAndEventId(String code, Long eventId) {
        Optional<TicketCodeEntity> fromDb = ticketCodeRepository.findByCodeAndEvent_Id(code, eventId);
        if(fromDb.isPresent()) {
            TicketCodeDto dtoFromDb = mapper.toDto(fromDb.get());
            dtoFromDb.setEventId(String.valueOf(eventId));
            return Optional.of(dtoFromDb);
        } else {
            return Optional.ofNullable(null);
        }
    }
}
