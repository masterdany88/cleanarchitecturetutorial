package pl.korbeldaniel.demo.repository.event;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.korbeldaniel.demo.domain.model.OrganizerDto;

import java.util.List;
import java.util.Optional;

@Repository
public interface EventRepository extends JpaRepository<EventEntity, Long> {
    String FIND_ID_BY_ON_MAIN_PAGE = "SELECT id FROM event_entity where on_main_page is true";
    String FIND_ID_BY_ON_MAIN_PAGE_AND_DOMAIN = "SELECT id FROM event_entity e\n" +
            "    left join organizer_entity o on e.organizer_domain=o.domain where on_main_page is true and domain like %?1%";

    @Query(value = FIND_ID_BY_ON_MAIN_PAGE, nativeQuery = true)
    Optional<Long> findIdByOnMainPageIsTrue();

    @Query(value = FIND_ID_BY_ON_MAIN_PAGE_AND_DOMAIN, nativeQuery = true)
    Optional<Long> findIdByOnMainPageAndOrganizer_DomainContains(String domain);

    List<EventEntity> findAllByActiveTrue();
    List<EventEntity> findAllByActiveTrueAndOrganizerEquals(OrganizerDto organizer);

}
