package pl.korbeldaniel.demo.repository.session;

import pl.korbeldaniel.demo.repository.event.EventEntity;
import pl.korbeldaniel.demo.repository.ticket.TicketCodeEntity;

import javax.persistence.*;

@Entity
public class StreamingSessionEntity {
    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne private EventEntity event;
    @ManyToOne private TicketCodeEntity ticketCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EventEntity getEvent() {
        return event;
    }

    public void setEvent(EventEntity event) {
        this.event = event;
    }

    public TicketCodeEntity getTicketCode() {
        return ticketCode;
    }

    public void setTicketCode(TicketCodeEntity ticketCode) {
        this.ticketCode = ticketCode;
    }
}
