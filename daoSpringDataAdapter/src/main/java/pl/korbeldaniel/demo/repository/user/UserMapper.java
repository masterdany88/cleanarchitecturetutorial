package pl.korbeldaniel.demo.repository.user;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.korbeldaniel.demo.domain.model.UserDto;


@Mapper
public interface UserMapper {
    UserDto toDto(UserEntity entity);
    UserEntity toEntity(UserDto dto);
    UserMapper INSTANCE = Mappers.getMapper( UserMapper.class );
}
