package pl.korbeldaniel.demo.repository.event;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.korbeldaniel.demo.domain.model.event.BuyTicketUrlDto;
import pl.korbeldaniel.demo.domain.model.event.EventDto;

@Mapper
public interface EventMapper {
    EventDto toDto(EventEntity entity);
    EventEntity toEntity(EventDto dto);

    EventMapper INSTANCE = Mappers.getMapper( EventMapper.class );
}
