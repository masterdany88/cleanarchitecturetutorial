package pl.korbeldaniel.demo.repository.user;

import org.springframework.stereotype.Component;
import pl.korbeldaniel.demo.domain.model.UserDto;
import pl.korbeldaniel.demo.usecase.port.dao.UserDao;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public
class UserDaoImpl implements UserDao {
    private final UserRepository repository;
    private final UserMapper mapper;

    public UserDaoImpl(UserRepository repository) {
        this.repository = repository;
        this.mapper = UserMapper.INSTANCE;
    }

    @Override
    public Optional<UserDto> findById(Long id) {
        Optional<UserEntity> fromDb = repository.findById(id);
        return Optional.ofNullable(mapper.toDto(fromDb.get()));
    }

    @Override
    public UserDto save(UserDto dto) {
        UserEntity fromDb = repository.save(mapper.toEntity(dto));
        return mapper.toDto(fromDb);
    }

    @Override
    public Collection<UserDto> findAll() {
        return repository.findAll().stream()
                .map(mapper::toDto)
                .collect(Collectors.toList());
    }
}
