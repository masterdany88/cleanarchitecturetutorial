package pl.korbeldaniel.demo.repository.ticket;

import pl.korbeldaniel.demo.domain.model.ticket.TicketCodeModel;
import pl.korbeldaniel.demo.repository.event.EventEntity;

import javax.persistence.*;

@Entity
@Table(uniqueConstraints={
        @UniqueConstraint(columnNames = {"code", "event_id"})
})
public class TicketCodeEntity implements TicketCodeModel {
    @Id
    private String code;
    @ManyToOne
    private EventEntity event;

    public TicketCodeEntity() {
    }
    public TicketCodeEntity(String code, EventEntity event) {
        this.code = code;
        this.event = event;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public EventEntity getEvent() {
        return event;
    }

    public void setEvent(EventEntity event) {
        this.event = event;
    }
}
