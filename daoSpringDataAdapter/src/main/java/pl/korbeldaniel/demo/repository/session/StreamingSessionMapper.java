package pl.korbeldaniel.demo.repository.session;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.korbeldaniel.demo.domain.model.StreamingSessionDto;
import pl.korbeldaniel.demo.domain.model.event.BuyTicketUrlDto;
import pl.korbeldaniel.demo.repository.event.BuyTicketUrlEntity;

@Mapper
public interface StreamingSessionMapper {
    StreamingSessionMapper INSTANCE = Mappers.getMapper(StreamingSessionMapper.class);
    StreamingSessionDto toDto(StreamingSessionEntity entity);
    StreamingSessionEntity toEntity(StreamingSessionDto dto);
}
