package pl.korbeldaniel.demo.repository.ticket;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TicketCodeRepository extends JpaRepository<TicketCodeEntity, String> {
    Optional<TicketCodeEntity> findByCodeAndEvent_Id(String code, Long eventId);
}
