package pl.korbeldaniel.demo.repository.event;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.korbeldaniel.demo.domain.model.event.BuyTicketUrlDto;

@Mapper
public interface BuyTicketUrlMapper {
    BuyTicketUrlDto toDto(BuyTicketUrlEntity entity);
    BuyTicketUrlEntity toEntity(BuyTicketUrlDto dto);
    BuyTicketUrlMapper INSTANCE = Mappers.getMapper(BuyTicketUrlMapper.class);
}
