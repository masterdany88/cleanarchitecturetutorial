package pl.korbeldaniel.demo.repository.event;

import pl.korbeldaniel.demo.domain.model.OrganizerDto;
import pl.korbeldaniel.demo.domain.model.event.EventModel;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class EventEntity implements EventModel {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @OneToMany(mappedBy="event")
    private List<BuyTicketUrlEntity> buyTicketUrls;
    private Date date;
    private Date time;
    @Lob
    @Column(name="pageContent", length=1000)
    private String pageContent;
    private boolean active;
    private boolean onMainPage;
    @Lob @Basic(fetch = FetchType.LAZY)
    private byte[] topBannerImage;
    @Lob @Basic(fetch = FetchType.LAZY)
    private byte[] coverImage;
    @Enumerated(EnumType.STRING)
    private OrganizerDto organizer;
    private boolean manyLoginWithSameCodeAllowed;

    public EventEntity() {
        // JPA requries it
    }

    public EventEntity(long id, String name, Date date, Date time, String pageContent, boolean active, boolean onMainPage, OrganizerDto organizer) {
        this.id = id;
        this.name = name;
        this.date = date;
        this.time = time;
        this.pageContent = pageContent;
        this.active = active;
        this.onMainPage = onMainPage;
        this.organizer = organizer;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<BuyTicketUrlEntity> getBuyTicketUrls() {
        return buyTicketUrls;
    }

    public void setBuyTicketUrls(List<BuyTicketUrlEntity> buyTicketUrls) {
        this.buyTicketUrls = buyTicketUrls;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getPageContent() {
        return pageContent;
    }

    public void setPageContent(String pageContent) {
        this.pageContent = pageContent;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isOnMainPage() {
        return onMainPage;
    }

    public void setOnMainPage(boolean onMainPage) {
        this.onMainPage = onMainPage;
    }

    public byte[] getTopBannerImage() {
        return topBannerImage;
    }

    public void setTopBannerImage(byte[] topBannerImage) {
        this.topBannerImage = topBannerImage;
    }

    public byte[] getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(byte[] coverImage) {
        this.coverImage = coverImage;
    }

    public OrganizerDto getOrganizer() {
        return organizer;
    }

    public void setOrganizer(OrganizerDto organizer) {
        this.organizer = organizer;
    }

    public boolean isManyLoginWithSameCodeAllowed() {
        return manyLoginWithSameCodeAllowed;
    }

    public void setManyLoginWithSameCodeAllowed(boolean manyLoginWithSameCodeAllowed) {
        this.manyLoginWithSameCodeAllowed = manyLoginWithSameCodeAllowed;
    }
}
