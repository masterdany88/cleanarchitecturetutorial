package pl.korbeldaniel.demo.payment.dep;

import pl.korbeldaniel.demo.domain.model.payment.PaymentMethodsResponse;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

public interface ApiResource {
    @GET
    @Path("/api/v1/payment/methods/{lang}")
    @Produces({ "application/json" })
    PaymentMethodsResponse apiV1PaymentMethodsLangGet(@PathParam("lang") String lang);
}
