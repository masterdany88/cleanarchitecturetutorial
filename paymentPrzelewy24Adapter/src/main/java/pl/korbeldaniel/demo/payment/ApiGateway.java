package pl.korbeldaniel.demo.payment;

import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.client.proxy.WebResourceFactory;
import pl.korbeldaniel.demo.domain.model.payment.PaymentMethodsResponse;
import pl.korbeldaniel.demo.payment.dep.ApiResource;
import pl.korbeldaniel.demo.usecase.buy.PaymentPort;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

public class ApiGateway implements PaymentPort {
    private Client client = ClientBuilder.newClient();

    public PaymentMethodsResponse getPaymentsMethods() {
        WebTarget apiTarget = client.target("https://sandbox.przelewy24.pl/");
        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("113571", "6bff21d4173e27217a90fe295aee6b34");
        apiTarget.register(feature);
        ApiResource apiResource = WebResourceFactory.newResource(ApiResource.class, apiTarget);
        return apiResource.apiV1PaymentMethodsLangGet("pl");
    }
}
