package pl.korbeldaniel.demo.domain.model.ticket;

public class TicketCodeDto implements TicketCodeModel {
    private String eventId;
    private String code;

    public TicketCodeDto() {
    }

    public TicketCodeDto(String eventId, String code) {
        this.eventId = eventId;
        this.code = code;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
