package pl.korbeldaniel.demo.domain.model.event;

import pl.korbeldaniel.demo.domain.model.OrganizerDto;

import java.util.Date;
import java.util.List;

public class EventDto implements EventModel {
    private Long id;
    private String name;
    private List<BuyTicketUrlDto> buyTicketUrls;
    private Date date;
    private Date time;
    private String pageContent;
    private boolean active;
    private boolean onMainPage;
    private OrganizerDto organizer;
    private boolean manyLoginWithSameCodeAllowed;

    public EventDto() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<BuyTicketUrlDto> getBuyTicketUrls() {
        return buyTicketUrls;
    }

    public void setBuyTicketUrls(List<BuyTicketUrlDto> buyTicketUrls) {
        this.buyTicketUrls = buyTicketUrls;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getPageContent() {
        return pageContent;
    }

    public void setPageContent(String pageContent) {
        this.pageContent = pageContent;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isOnMainPage() {
        return onMainPage;
    }

    public void setOnMainPage(boolean onMainPage) {
        this.onMainPage = onMainPage;
    }

    public OrganizerDto getOrganizer() {
        return organizer;
    }

    public void setOrganizer(OrganizerDto organizer) {
        this.organizer = organizer;
    }

    public boolean isManyLoginWithSameCodeAllowed() {
        return manyLoginWithSameCodeAllowed;
    }

    public void setManyLoginWithSameCodeAllowed(boolean manyLoginWithSameCodeAllowed) {
        this.manyLoginWithSameCodeAllowed = manyLoginWithSameCodeAllowed;
    }
}
