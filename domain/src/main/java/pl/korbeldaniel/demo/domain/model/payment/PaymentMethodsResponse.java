package pl.korbeldaniel.demo.domain.model.payment;

import java.util.List;

public class PaymentMethodsResponse {

    private List<PaymentMethodResponse> data;
    private String error;
    private String responseCode;

    public List<PaymentMethodResponse> getData() {
        return data;
    }

    public void setData(List<PaymentMethodResponse> data) {
        this.data = data;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }
}
