package pl.korbeldaniel.demo.domain.model.event;

public interface EventModel {
    Long getId();

    String getName();
}
