package pl.korbeldaniel.demo.domain.model.ticket;

public interface TicketCodeModel {

    String getCode();

    void setCode(String codeId);
}
