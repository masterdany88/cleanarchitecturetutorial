package pl.korbeldaniel.demo.domain.model;

public enum OrganizerDto {
    TEAM_STREAM("teamstream", true),
    KANAL_KOMEDIOWY("kanalkomediowy", false);

    private final String domain;
    private final boolean main;

    OrganizerDto(String domain, boolean main) {
        this.domain = domain;
        this.main = main;
    }

    public static OrganizerDto getMain() {
        for (OrganizerDto value : OrganizerDto.values()) {
            if(value.isMain()) {
                return value;
            }
        }
        throw new IllegalArgumentException("There is no main Organizer");
    }

    public static OrganizerDto getByFullDomain(String domain) {
        for (OrganizerDto value : OrganizerDto.values()) {
            if(domain.equals(value.getDomainWithContryCode())
                || domain.equals("www." + value.getDomainWithContryCode())
            ) {
                return value;
            }
        }
        throw new IllegalArgumentException("Domain '" + domain + "' not recognized");
    }

    public boolean isMain() {
        return main;
    }

    public String getDomain() {
        return domain;
    }

    public String getDomainWithContryCode() {
        return domain + ".pl";
    }
}

