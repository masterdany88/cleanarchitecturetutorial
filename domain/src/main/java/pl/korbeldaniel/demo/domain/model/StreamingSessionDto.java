package pl.korbeldaniel.demo.domain.model;

import pl.korbeldaniel.demo.domain.model.event.EventDto;
import pl.korbeldaniel.demo.domain.model.ticket.TicketCodeDto;

public class StreamingSessionDto {
    private Long id;
    private EventDto event;
    private TicketCodeDto ticketCode;

    public StreamingSessionDto() {
    }

    public StreamingSessionDto(TicketCodeDto ticketCode) {
        this.ticketCode = ticketCode;
    }

    public StreamingSessionDto(Long id, EventDto event, TicketCodeDto ticketCode) {
        this.id = id;
        this.event = event;
        this.ticketCode = ticketCode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EventDto getEvent() {
        return event;
    }

    public void setEvent(EventDto event) {
        this.event = event;
    }

    public TicketCodeDto getTicketCode() {
        return ticketCode;
    }

    public void setTicketCode(TicketCodeDto ticketCode) {
        this.ticketCode = ticketCode;
    }
}
