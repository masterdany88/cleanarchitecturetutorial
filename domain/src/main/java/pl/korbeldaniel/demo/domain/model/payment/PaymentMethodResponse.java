package pl.korbeldaniel.demo.domain.model.payment;

public class PaymentMethodResponse {

    private String name = null;
    private Integer id = null;
    private Boolean status = null;
    private String imgUrl = null;
    private String mobileImgUrl = null;
    private Boolean mobile = null;
    private AvailabilityHoursResponse availabilityHours = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PaymentMethodResponse name(String name) {
        this.name = name;
        return this;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public PaymentMethodResponse id(Integer id) {
        this.id = id;
        return this;
    }

    public Boolean isStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public PaymentMethodResponse status(Boolean status) {
        this.status = status;
        return this;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public PaymentMethodResponse imgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
        return this;
    }

    public String getMobileImgUrl() {
        return mobileImgUrl;
    }

    public void setMobileImgUrl(String mobileImgUrl) {
        this.mobileImgUrl = mobileImgUrl;
    }

    public PaymentMethodResponse mobileImgUrl(String mobileImgUrl) {
        this.mobileImgUrl = mobileImgUrl;
        return this;
    }

    public Boolean isMobile() {
        return mobile;
    }

    public void setMobile(Boolean mobile) {
        this.mobile = mobile;
    }

    public PaymentMethodResponse mobile(Boolean mobile) {
        this.mobile = mobile;
        return this;
    }

    public AvailabilityHoursResponse getAvailabilityHours() {
        return availabilityHours;
    }

    public void setAvailabilityHours(AvailabilityHoursResponse availabilityHours) {
        this.availabilityHours = availabilityHours;
    }

    public PaymentMethodResponse availabilityHours(AvailabilityHoursResponse availabilityHours) {
        this.availabilityHours = availabilityHours;
        return this;
    }

}
