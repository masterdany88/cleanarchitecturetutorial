package pl.korbeldaniel.demo.domain.model.payment;

public class AvailabilityHoursResponse   {
  
  private String mondayToFriday = "00-24";
  private String saturday = "unavailable";
  private String sunday = "00-24";

  public String getMondayToFriday() {
    return mondayToFriday;
  }

  public void setMondayToFriday(String mondayToFriday) {
    this.mondayToFriday = mondayToFriday;
  }

  public AvailabilityHoursResponse mondayToFriday(String mondayToFriday) {
    this.mondayToFriday = mondayToFriday;
    return this;
  }

  public String getSaturday() {
    return saturday;
  }

  public void setSaturday(String saturday) {
    this.saturday = saturday;
  }

  public AvailabilityHoursResponse saturday(String saturday) {
    this.saturday = saturday;
    return this;
  }

  public String getSunday() {
    return sunday;
  }

  public void setSunday(String sunday) {
    this.sunday = sunday;
  }

  public AvailabilityHoursResponse sunday(String sunday) {
    this.sunday = sunday;
    return this;
  }
}
