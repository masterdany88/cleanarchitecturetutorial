package pl.korbeldaniel.demo.domain.model;

public interface UserModel {
    Long getId();
    String getName();
    String getEmail();
    int getAge();
}
