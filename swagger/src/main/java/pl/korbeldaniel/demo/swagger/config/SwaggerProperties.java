package pl.korbeldaniel.demo.swagger.config;

import io.swagger.models.Contact;
import io.swagger.models.Info;
import io.swagger.models.License;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ConfigurationProperties(prefix = "swagger")
@PropertySource("swagger.properties")
@Data
public class SwaggerProperties {

    private String host;
    private String resourcePackage;
    private String title;
    private String description;
    private String version;
    private String termsOfService;
    private ContactProperties contact;
    private LicenseProperties license;

    @Getter
    @Setter
    public static class ContactProperties {
        private String name;
        private String url;
        private String email;
    }

    @Getter
    @Setter
    public static class LicenseProperties {
        private String name;
        private String url;
    }

    public Info getInfo() {
        return new Info().title(title).description(description).version(version).termsOfService(termsOfService)
                .contact(new Contact().name(contact.getName()).url(contact.getUrl()).email(contact.getEmail()))
                .license(new License().name(license.getName()).url(license.getUrl()));
    }
}
