package pl.korbeldaniel.demo.swagger.config;

import io.swagger.converter.ModelConverters;
import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.config.SwaggerConfigLocator;
import io.swagger.jaxrs.config.SwaggerContextService;
import io.swagger.jaxrs.listing.ApiListingResource;
import io.swagger.jaxrs.listing.SwaggerSerializers;
import io.swagger.models.Model;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;
import java.util.Map;

@Configuration
public class SwaggerConfig extends ResourceConfig {

    private final SwaggerProperties swaggerProperties;

    public SwaggerConfig(final SwaggerProperties swaggerProperties) {
        super();
        this.swaggerProperties = swaggerProperties;
        register(ApiListingResource.class);
        register(SwaggerSerializers.class);
        configureSwagger();
    }

    private void configureSwagger() {
        BeanConfig beanConfig = createConfiguredBeanConfig();
        SwaggerContextService swaggerContextService = createSwaggerContextService().withSwaggerConfig(beanConfig)
                .withScanner(beanConfig).initConfig().initScanner();

        registerAdditionalModels(swaggerContextService);
    }

    private BeanConfig createConfiguredBeanConfig() {
        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setHost(swaggerProperties.getHost());
        beanConfig.setResourcePackage(swaggerProperties.getResourcePackage());
        beanConfig.setInfo(swaggerProperties.getInfo());
        return beanConfig;
    }

    private SwaggerContextService createSwaggerContextService() {
        SwaggerContextService swaggerContextService = new SwaggerContextService();
        SwaggerConfigLocator.getInstance().putSwagger(SwaggerContextService.CONFIG_ID_DEFAULT,
                swaggerContextService.getSwagger());
        return swaggerContextService;
    }

    private void registerAdditionalModels(SwaggerContextService swaggerContextService) {
        // registerAdditionalModel(swaggerContextService, EventMessageBase.class);
        // registerAdditionalModel(swaggerContextService, DeviceMetaDataBase.class);
    }

    private void registerAdditionalModel(SwaggerContextService swaggerContextService, Class className) {
        final Map<String, Model> stringModelMap = ModelConverters.getInstance().readAll(className);
        for (Map.Entry<String, Model> stringModelEntry : stringModelMap.entrySet()) {
            swaggerContextService.getSwagger().addDefinition(stringModelEntry.getKey(), stringModelEntry.getValue());
        }
    }
}
