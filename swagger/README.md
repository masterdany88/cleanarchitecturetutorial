#1 Swagger json:
http://localhost:8088/swagger.json

#2 Swagger ui:
http://localhost:8088/api-docs/webjars/swagger-ui/index.html?url=/swagger.json

Outside swagger editor: https://editor.swagger.io/
