package pl.korbeldaniel.demo.usecase.event;

import pl.korbeldaniel.demo.usecase.port.dao.EventDao;
import pl.korbeldaniel.demo.domain.model.event.EventDto;

public final class UpdateEventUc {
    private final EventDao dao;

    public UpdateEventUc(final EventDao dao) {
        this.dao = dao;
    }

    public void update(final EventDto dto) {
        dao.update(dto);
    }

}