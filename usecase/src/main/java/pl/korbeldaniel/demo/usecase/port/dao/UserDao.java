package pl.korbeldaniel.demo.usecase.port.dao;

import pl.korbeldaniel.demo.domain.model.UserDto;

import java.util.Collection;
import java.util.Optional;

public interface UserDao {
    Optional<UserDto> findById(Long id);
    UserDto save(UserDto user);
    Collection<UserDto> findAll();
}
