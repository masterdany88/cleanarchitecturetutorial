package pl.korbeldaniel.demo.usecase.buy;

import pl.korbeldaniel.demo.domain.model.payment.PaymentMethodsResponse;

public interface PaymentPort {
    PaymentMethodsResponse getPaymentsMethods();
}
