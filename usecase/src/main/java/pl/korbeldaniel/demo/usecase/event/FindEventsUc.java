package pl.korbeldaniel.demo.usecase.event;

import pl.korbeldaniel.demo.domain.model.OrganizerDto;
import pl.korbeldaniel.demo.usecase.port.dao.EventDao;
import pl.korbeldaniel.demo.usecase.port.dao.TicketCodeDao;
import pl.korbeldaniel.demo.domain.model.event.EventDto;
import pl.korbeldaniel.demo.domain.model.ticket.TicketCodeDto;

import java.util.List;
import java.util.Optional;

public final class FindEventsUc {
    private final EventDao eventDao;
    private final TicketCodeDao ticketCodeDao;

    public FindEventsUc(final EventDao eventDao, TicketCodeDao ticketCodeDao) {
        this.eventDao = eventDao;
        this.ticketCodeDao = ticketCodeDao;
    }

    public Optional<EventDto> findById(final Long id) {
        return eventDao.findById(id);
    }

    public List<EventDto> findAll() {
        return eventDao.findAll();
    }

    public List<EventDto> findSummaryOfAllActiveByOrganizer(OrganizerDto organizer) {
        if(organizer.isMain()) {
            return eventDao.findSummaryOfAllActive();
        } else {
            return eventDao.findSummaryOfAllActiveByOrganizer(organizer);
        }
    }

    public EventDto findSummaryById(Long id) {
        return eventDao.findSummaryById(id);
    }

    public Optional<EventDto> findByIdAndTicketCode(Long eventId, String ticketCode) {
        Optional<TicketCodeDto> byCodeAndEventId = ticketCodeDao.findByCodeAndEventId(ticketCode, eventId);
        if (byCodeAndEventId.isPresent()) {
            return findById(eventId);
        } else {
            return Optional.empty();
        }
    }
}