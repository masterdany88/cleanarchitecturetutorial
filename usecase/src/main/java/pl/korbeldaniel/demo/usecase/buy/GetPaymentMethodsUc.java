package pl.korbeldaniel.demo.usecase.buy;

import pl.korbeldaniel.demo.domain.model.payment.PaymentMethodsResponse;

public class GetPaymentMethodsUc {
    private final PaymentPort paymentPort;

    public GetPaymentMethodsUc(PaymentPort paymentPort) {
        this.paymentPort = paymentPort;
    }

    public PaymentMethodsResponse getPaymentMethods() {
        return paymentPort.getPaymentsMethods();
    }
}
