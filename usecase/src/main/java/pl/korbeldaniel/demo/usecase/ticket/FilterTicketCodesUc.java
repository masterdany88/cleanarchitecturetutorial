package pl.korbeldaniel.demo.usecase.ticket;

import java.util.ArrayList;
import java.util.List;

public class FilterTicketCodesUc {

    public List<String> filter(List<String[]> codes) {
        codes.removeIf(code ->
                code[0].length() == 0
                        || code[0].equals("")
                        || code[0].contains("WebKit")
                        || code[0].contains("------")
                        || code[0].contains("csv")
                        || code[0].contains("content"));
        List<String> extractedCodes = new ArrayList<>();
        for(String[] a: codes) {
            String code = a[0];
            code = code.replaceAll(";","");
            code = code.replaceAll(" ","");
            code = code.replaceAll("\uFEFF","");
            code = code.trim();
            extractedCodes.add(code);
        }
        return extractedCodes;
    }
}
