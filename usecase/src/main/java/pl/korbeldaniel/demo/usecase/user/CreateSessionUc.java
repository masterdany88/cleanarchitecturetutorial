package pl.korbeldaniel.demo.usecase.user;

import pl.korbeldaniel.demo.usecase.port.dao.EventDao;
import pl.korbeldaniel.demo.usecase.port.dao.TicketCodeDao;
import pl.korbeldaniel.demo.usecase.port.dao.StreamingSessionDao;
import pl.korbeldaniel.demo.domain.model.StreamingSessionDto;
import pl.korbeldaniel.demo.domain.model.event.EventDto;
import pl.korbeldaniel.demo.domain.model.ticket.TicketCodeDto;

import java.util.Optional;

public class CreateSessionUc {
    private final StreamingSessionDao streamingSessionDao;
    private final TicketCodeDao ticketCodeDao;
    private final EventDao eventDao;

    public CreateSessionUc(final StreamingSessionDao streamingSessionDao, final TicketCodeDao ticketCodeDao, EventDao eventDao) {
        this.streamingSessionDao = streamingSessionDao;
        this.ticketCodeDao = ticketCodeDao;
        this.eventDao = eventDao;
    }

    public StreamingSessionDto createSession(StreamingSessionDto dto, String domain) {
//        Long currentEventId = eventDao.findById(Long.valueOf(dto.getTicketCode().getEventId())).get();
//        Long currentEventId = eventDao.findCurrentEventId().get();
        EventDto event = eventDao.findById(Long.valueOf(dto.getTicketCode().getEventId())).get();
        Optional<TicketCodeDto> ticketByCode = ticketCodeDao.findByCodeAndEventId(
                dto.getTicketCode().getCode(),
                Long.valueOf(dto.getTicketCode().getEventId()));
        if(ticketByCode.isPresent()) {
            dto.setTicketCode(ticketByCode.get());
            dto.setEvent(event);
            streamingSessionDao.deleteByTicketCode(dto.getTicketCode().getCode());
            return streamingSessionDao.create(dto);
        } else {
            throw new IllegalArgumentException("Kod dostępu jest niepoprawny");
        }
    }
}
