package pl.korbeldaniel.demo.usecase.user;

import pl.korbeldaniel.demo.usecase.port.dao.UserDao;
import pl.korbeldaniel.demo.domain.model.UserDto;

import java.util.Collection;
import java.util.Optional;

public final class FindUsersUc {
    private final UserDao dao;

    public FindUsersUc(final UserDao dao) {
        this.dao = dao;
    }

    public Optional<UserDto> findById(final Long id) {
        return dao.findById(id);
    }

    public Collection<UserDto> findAll() {
        return dao.findAll();
    }
}