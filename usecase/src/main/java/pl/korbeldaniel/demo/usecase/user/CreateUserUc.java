package pl.korbeldaniel.demo.usecase.user;

import pl.korbeldaniel.demo.usecase.port.dao.UserDao;
import pl.korbeldaniel.demo.domain.model.UserDto;

public final class CreateUserUc {
    private final UserDao dao;

    public CreateUserUc(final UserDao dao) {
        this.dao = dao;
    }

    public UserDto createUser(final UserDto userDto) {
        return dao.save(userDto);
    }
}