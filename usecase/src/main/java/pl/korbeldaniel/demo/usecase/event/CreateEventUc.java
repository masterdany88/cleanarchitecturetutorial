package pl.korbeldaniel.demo.usecase.event;

import pl.korbeldaniel.demo.usecase.port.dao.EventDao;
import pl.korbeldaniel.demo.domain.model.event.EventDto;

public final class CreateEventUc {
    private final EventDao dao;

    public CreateEventUc(final EventDao dao) {
        this.dao = dao;
    }

    public EventDto create(final EventDto dto) {
        return dao.save(dto);
    }

}