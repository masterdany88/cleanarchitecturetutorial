package pl.korbeldaniel.demo.usecase.user;

import pl.korbeldaniel.demo.usecase.port.dao.StreamingSessionDao;
import pl.korbeldaniel.demo.domain.model.ResponseDto;

public class GetSessionValidityUc {
    private final StreamingSessionDao streamingSessionDao;

    public GetSessionValidityUc(StreamingSessionDao streamingSessionDao) {
        this.streamingSessionDao = streamingSessionDao;
    }

    public ResponseDto get(String id, String accessCode) {
        if(streamingSessionDao.findByIdAndTicketCode_code(Long.valueOf(id), accessCode) == null) {
            return new ResponseDto("Expired");
        } else {
            return new ResponseDto("Ok");
        }
    }
}
