package pl.korbeldaniel.demo.usecase.port.email;

public interface EmailServicePort {
    void sendTicketPurchasedConfirmationEmail(String to, String subject, String eventName, String accessCode);
}
