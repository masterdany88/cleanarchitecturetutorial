package pl.korbeldaniel.demo.usecase.port.dao;

import pl.korbeldaniel.demo.domain.model.OrganizerDto;
import pl.korbeldaniel.demo.domain.model.event.EventDto;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface EventDao {
    Optional<Long> findCurrentEventId();
    Optional<Long> findCurrentEventIdByWwwDomain(String domain);
    Optional<EventDto> findById(Long id);
    EventDto save(EventDto dto);
    void update(EventDto dto);
    List<EventDto> findAll();
    List<EventDto> findSummaryOfAllActive();
    List<EventDto> findSummaryOfAllActiveByOrganizer(OrganizerDto organizer);
    EventDto findSummaryById(Long id);
    void updateTopBannerById(byte[] image, Long id);
    void updateCoverById(byte[] image, Long id);
    byte[] findTopBannerById(Long id);
    byte[] findCoverById(Long id);

}
