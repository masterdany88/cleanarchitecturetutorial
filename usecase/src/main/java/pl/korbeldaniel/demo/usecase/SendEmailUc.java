package pl.korbeldaniel.demo.usecase;

import pl.korbeldaniel.demo.usecase.port.email.EmailServicePort;

public class SendEmailUc {
    private final EmailServicePort emailServicePort;

    public SendEmailUc(EmailServicePort emailServicePort) {
        this.emailServicePort = emailServicePort;
    }

    public void sendTicketPurchasedConfirmationEmail(String to, String topic, String eventName, String accessCode) {
        emailServicePort.sendTicketPurchasedConfirmationEmail(to, topic, eventName, accessCode);
    }
}
