package pl.korbeldaniel.demo.usecase.port.dao;

import pl.korbeldaniel.demo.domain.model.ticket.TicketCodeDto;

import java.util.List;
import java.util.Optional;

public interface TicketCodeDao {
    List<TicketCodeDto> saveAll(List<TicketCodeDto> codes, Long eventId);
    Optional<TicketCodeDto> findByCodeAndEventId(String code, Long eventId);

}
