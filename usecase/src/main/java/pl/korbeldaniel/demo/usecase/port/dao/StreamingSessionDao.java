package pl.korbeldaniel.demo.usecase.port.dao;

import pl.korbeldaniel.demo.domain.model.StreamingSessionDto;

public interface StreamingSessionDao {
    StreamingSessionDto create(StreamingSessionDto dto);
    StreamingSessionDto findByIdAndTicketCode_code(Long id, String ticketCode);
    void deleteByTicketCode(String ticketCode);
}
