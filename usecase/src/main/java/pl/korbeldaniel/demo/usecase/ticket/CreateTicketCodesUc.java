package pl.korbeldaniel.demo.usecase.ticket;

import pl.korbeldaniel.demo.usecase.port.dao.TicketCodeDao;
import pl.korbeldaniel.demo.domain.model.ticket.TicketCodeDto;

import java.util.List;

public class CreateTicketCodesUc {
    private TicketCodeDao dao;

    public CreateTicketCodesUc(TicketCodeDao dao) {
        this.dao = dao;
    }

    public List<TicketCodeDto> createTickets(final List<TicketCodeDto> codes, Long eventId) {
        return dao.saveAll(codes, eventId);
    }

}
